package com.ingenio.eprashasan.components;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class OfficeDetailsComponent extends QAFWebComponent{
	
	@FindBy(locator = "xpath=//select[@id='yearcombo']")
    public DropdownListWebElement dropdownAcademicYear;
	
	@FindBy(locator = "xpath=//select[@id='isProvisionalAdmission']")
    public DropdownListWebElement dropdownRegistrationType;
	
	@FindBy(locator = "xpath=//input[@id='registrationNo1']")
    public QAFWebElement textboxRegistrationNumber;
	
	@FindBy(locator = "xpath=//select[@name='admissionday']")
    public DropdownListWebElement dropdownAdmissionDay;
	
	@FindBy(locator = "xpath=//select[@name='admissionmonth']")
    public DropdownListWebElement dropdownAdmissionMonth;
	
	@FindBy(locator = "xpath=//select[@name='admissionyear']")
    public DropdownListWebElement dropdownAdmissionYear;
	
	@FindBy(locator = "xpath=//input[@id='studentFormNumberId']")
    public QAFWebElement textboxStudentFormNumber;
	
	@FindBy(locator = "xpath=//select[@name='standardName']")
    public DropdownListWebElement dropdownClass;
	
	@FindBy(locator = "xpath=//select[@name='medium']")
    public DropdownListWebElement dropdownMedium;
	
	@FindBy(locator = "xpath=//select[@id='initialNameId1']")
    public DropdownListWebElement dropdownInitialName;
	
	@FindBy(locator = "xpath=//input[@id='firstNameId']")
    public QAFWebElement textboxFirstName;
	
	@FindBy(locator = "xpath=//input[@id='middleNameId']")
    public QAFWebElement textboxMiddleName;
	
	@FindBy(locator = "xpath=//input[@id='lastNameId']")
    public QAFWebElement textboxLastName;
	
	@FindBy(locator = "xpath=//select[@name='birthday']")
    public DropdownListWebElement dropdownBirthDay;
	
	@FindBy(locator = "xpath=//select[@name='birthmonth']")
    public DropdownListWebElement dropdownBirthMonth;
	
	@FindBy(locator = "xpath=//select[@name='birthyear']")
    public DropdownListWebElement dropdownBirthYear;
	
	@FindBy(locator = "xpath=//select[@id='genId']")
    public DropdownListWebElement dropdownGender;
	
	@FindBy(locator = "xpath=//select[@id='castcombo'] | //input[@name='casteName']")
    public DropdownListWebElement dropdownCaste;
	
	@FindBy(locator = "xpath=//select[@id='categoryComboId']")
    public DropdownListWebElement dropdownCategory;
	
	@FindBy(locator = "xpath=//select[@id='religioncombo']")
    public DropdownListWebElement dropdownReligion;
	
	@FindBy(locator = "xpath=//input[@id='mmobile']")
    public QAFWebElement textboxMobileNo;
	
	
//	for Staff Basic Details 
	
	@FindBy(locator = "xpath=//select[@id='staffRole']")
    public DropdownListWebElement dropdownStaffRole;
	
	@FindBy(locator = "xpath=//select[@id='staffschoolIdcombo']")
    public DropdownListWebElement dropdownStaffSchoolType;
	
	@FindBy(locator = "xpath=//select[@id='syearcombo']")
    public DropdownListWebElement dropdownJoiningYear;
	
	@FindBy(locator = "xpath=//input[@id='sregNo']")
    public QAFWebElement textboxStaffRegNo;
	
	@FindBy(locator = "xpath=//select[@id='joiningdayID']")
    public DropdownListWebElement dropdownJoiningDay;
	
	@FindBy(locator = "xpath=//select[@id='joiningmonthID']")
    public DropdownListWebElement dropdownJoiningMonth;
	
	@FindBy(locator = "xpath=//select[@id='entryyearId']")
    public DropdownListWebElement dropdownJoiningEntryYear;
	
	@FindBy(locator = "xpath=//select[@id='stafftypecombo']")
    public DropdownListWebElement dropdownStaffType;
	
	@FindBy(locator = "xpath=//select[@id='designationcombo']")
    public DropdownListWebElement dropdownStaffDesignation;
	
	@FindBy(locator = "xpath=//select[@id='departmentcombo']")
    public DropdownListWebElement dropdownStaffDepartment;
	
	@FindBy(locator = "xpath=//select[@id='staffPaymenttypecombo']")
    public DropdownListWebElement dropdownStaffServiceType;
	
	

	
	public OfficeDetailsComponent(QAFExtendedWebElement parent, String locator) {
		super(parent, locator);
		// TODO Auto-generated constructor stub
	}
	public OfficeDetailsComponent(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}
	
}
