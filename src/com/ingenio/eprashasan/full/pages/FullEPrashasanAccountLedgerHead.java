package com.ingenio.eprashasan.full.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountLedgerHead extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//img[@src=\"/ePrashasanE1/resources/account/img/add.png\"]")
	public QAFWebElement imgNewEntry;
	
	@FindBy(locator = "xpath=//input[@id=\"ledgerHeadName\"]")
	public QAFWebElement textboxAddLedgerHead;
	
	@FindBy(locator = "xpath=(//img[@src=\"/ePrashasanE1/resources/account/img/edit.png\"])[1]")
	public QAFWebElement imgEdit;
	
	@FindBy(locator = "xpath=((//img[@src=\"/ePrashasanE1/resources/account/img/delete.png\"])[1]")
	public QAFWebElement imgDelete;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
