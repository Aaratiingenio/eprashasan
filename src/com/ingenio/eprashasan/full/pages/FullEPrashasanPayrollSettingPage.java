package com.ingenio.eprashasan.full.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollSettingPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=(//input[@id=\"flagArr\"])[1]")
	public QAFWebElement checkboxSandwitchLeavewiseDeduction;
	
	@FindBy(locator = "xpath=(//input[@id=\"flagArr\"])[2]")
	public QAFWebElement checkboxHalfDayCutInPayroll;
	
	@FindBy(locator = "xpath=(//input[@id=\"flagArr\"])[3]")
	public QAFWebElement checkboxAttendanceSetting;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSaveSetting;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
