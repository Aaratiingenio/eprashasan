package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanSettingsPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	//Main Menu Locators
	@FindBy(locator = "xpath=//input[@value='Admission & Provisional Admission'] | //input[@id='one1']")
    public QAFWebElement tabAdmissionAndProvisionalAdmission;
	
	@FindBy(locator = "xpath=//input[@value='SMS-E-Mail'] | //input[@value='संदेश आणि इमेल']")
    public QAFWebElement tabSMSEMail;
	
	@FindBy(locator = "xpath=//input[@value='User Authentication'] | //input[@value='वापरकर्त्याचे अधिकार']") 
    public QAFWebElement tabUserAuthentication;
	
	@FindBy(locator = "xpath=//input[@value='Certificate(TC & Bonafide)'] | //input[@value='प्रमाणपत्र ( दाखला व बोनाफाईड)']")
    public QAFWebElement tabCertificateTCAndBonafide;
	
	@FindBy(locator = "xpath=//input[@value='Enquiry'] | //input[@value='चौकशी']")
    public QAFWebElement tabEnquiry;
	
	@FindBy(locator = "xpath=//input[@value='Export/Import Data For Provisional Admission'] | //input[@value='तात्पुरत्या माहितीची आयात निर्यात']")
    public QAFWebElement tabExportImportDataForProvisionalAdmission;
	
	@FindBy(locator = "xpath=//input[@value='Extra Curricular Activity'] | //input[@value='अभ्यासेतर उपक्रम']")
    public QAFWebElement tabExtraCurricularActivity;
	
	@FindBy(locator = "xpath=//input[@value='Class Management'] | //input[@value='वर्ग व्यवस्थापन']")
    public QAFWebElement tabClassManagement;

	@FindBy(locator = "xpath=//input[@value='Subject Section'] | //input[@value='विषय  व्यवस्थापन']")
    public QAFWebElement tabSubjectSection;
	
	@FindBy(locator = "xpath=//input[@value='Transport Setting'] | ")
    public QAFWebElement tabTransportSetting;
	
	@FindBy(locator = "xpath=//input[@value='Challan Setting'] | //input[@id='one11']")
    public QAFWebElement tabChallanSetting;
	
	@FindBy(locator = "xpath=//input[@value='Student Documents'] | ")
    public QAFWebElement tabStudentDocuments;
	
	
	//1st Sub-Menu Admission & Provisional Admission Locators
	@FindBy(locator = "xpath=//input[@value='Academic Year'] | //input[@value='वर्ष']")
    public QAFWebElement subTabAcademicYear;
	
	@FindBy(locator = "xpath=//input[@value='Standard'] | //input[@value='वर्ग']")
    public QAFWebElement subTabStandard;
	
	@FindBy(locator = "xpath=//input[@value='Division'] | //input[@value='तुकडी']")
    public QAFWebElement subTabDivision;
	
	@FindBy(locator = "xpath=//input[@value='Religion'] | //input[@value='धर्म']")
    public QAFWebElement subTabReligion;
	
	@FindBy(locator = "xpath=//input[@value='Caste Section'] | //input[@value='जात']")
    public QAFWebElement subTabCasteSection;
	
	@FindBy(locator = "xpath=//input[@value='Category'] | //input[@value='वर्ग-जात']")
    public QAFWebElement subTabCategory;
	
	@FindBy(locator = "xpath=//input[@value='Concession'] | //input[@value='सवलत']")
    public QAFWebElement subTabConcession;
	
	@FindBy(locator = "xpath=//input[@value='Minority Setting'] | //input[@id='firstSubMenu8']")
    public QAFWebElement subTabMinoritySetting;
	
	@FindBy(locator = "xpath=//input[@value='Dynamic Fields'] | //input[@value='इच्छित माहिती']")
    public QAFWebElement subTabDynamicFields;
	
	@FindBy(locator = "xpath=//input[@value='Entrance/Token Fees'] | //input[@value='शुल्क सेटिंग']")
    public QAFWebElement subTabEntranceTokenFees;

	@FindBy(locator = "xpath=//input[@value='Admission Form Setting'] | //input[@value='प्रवेश शुल्क सेटिंग']")
    public QAFWebElement subTabAdmissionFormSetting;
	
	@FindBy(locator = "xpath=//input[@value='Religion(Super Master 1)'] | //input[@value='धर्म(सुपर मास्टर १)']")
    public QAFWebElement subTabReligionSuperMaster1;
	
	@FindBy(locator = "xpath=//input[@value='Religion(Super Master 2)'] | //input[@value='धर्म(सुपर मास्टर २)']")
    public QAFWebElement subTabReligionSuperMaster2;
	
	@FindBy(locator = "xpath=//input[@value='Religion(Super Master 3)'] | //input[@value='धर्म(सुपर मास्टर ३)']")
    public QAFWebElement subTabReligionSuperMaster3;

	
	//2nd Sub-Menu SMS-E-Mail Locators
	@FindBy(locator = "xpath=//input[@value='SMS Setting'] | //input[@value='संदेश सेटिंग']")
    public QAFWebElement subTabSMSSetting;
	
	@FindBy(locator = "xpath=//input[@value='Mail Setting'] | //input[@value='मेल सेटिंग']")
    public QAFWebElement subTabMailSetting;
	
	@FindBy(locator = "xpath=//input[@value='Back Up'] | //input[@value='पावती सेटिंग']")
    public QAFWebElement subTabBackUp;
	
	@FindBy(locator = "xpath=//input[@value='SMS/EMAIL Permission'] | //input[@value='संदेश /इमेल परवाणगी']")
    public QAFWebElement subTabSMSEMAILPermission;
	
	@FindBy(locator = "xpath=//input[@value='Birthday SMS'] | //input[@value='वाढदिवस संदेश']")
    public QAFWebElement subTabBirthdaySMS;
	
	@FindBy(locator = "xpath=//input[@value='Remaining Amount Show/hide'] | //input[@value='बॅक-अप व्यवस्थापन']")
    public QAFWebElement subTabRemainingAmountShowhide;
	
	@FindBy(locator = "xpath=//input[@value='Present Student Classwise SMS'] | //input[@value='वर्गानुसार हजर विद्यार्थीसाठी संदेश']")
    public QAFWebElement subTabPresentStudentClasswiseSMS;
	
	@FindBy(locator = "xpath=//input[@value='Absent Student Classwise SMS'] | //input[@value='वर्गानुसार गैरहजर विद्यार्थीसाठी संदेश']")
    public QAFWebElement subTabAbsentStudentClasswiseSMS;
	
	@FindBy(locator = "xpath=//input[@value='Present Student Subjectwise SMS'] | //input[@value='विषयानुसार हजर विद्यार्थीसाठी संदेश']")
    public QAFWebElement subTabPresentStudentSubjectwiseSMS;
	
	@FindBy(locator = "xpath=//input[@value='Absent Student Subjectwise SMS'] | //input[@value='विषयानुसार गैरहजर विद्यार्थीसाठी संदेश']")
    public QAFWebElement subTabAbsentStudentSubjectwiseSMS;
	
	@FindBy(locator = "xpath=//input[@value='Upload Birthday Greeting'] | //input[@value='अपलोड बर्थडे ग्रीटिंग']")
    public QAFWebElement subTabUploadBirthdayGreeting;
	
	
	//3rd Sub-Menu User Authentication Locators
	@FindBy(locator = "xpath=//input[@value='No of User'] | //input[@value=' एकूण वापरकर्ता']")
    public QAFWebElement subTabNoOfUser;
	
	@FindBy(locator = "xpath=//input[@value='User'] | //input[@value='वापरकर्ता']")
    public QAFWebElement subTabUser;
	
	@FindBy(locator = "xpath=//input[@value='Exam-Class Users'] | //input[@value='वर्ग-परीक्षा वापरकर्ता ']")
    public QAFWebElement subTabExamClassUsers;
	
	@FindBy(locator = "xpath=//input[@value='Welcome Note'] | //input[@value='स्वागत वाक्य']")
    public QAFWebElement subTabWelcomeNote;
	
	@FindBy(locator = "xpath=//input[@value='Upload Setting Image'] | //input[@value='अपलोड सेटींग इमेज']")
    public QAFWebElement subTabUploadSettingImage;
	
	
	//4th Sub-Menu Certificate(TC & Bonafide) Locators
	@FindBy(locator = "xpath=//input[@value='TC Setting'] | //input[@value='टिसी सेटिंग']")
    public QAFWebElement subTabTCSetting;
	
	@FindBy(locator = "xpath=//input[@value='Upload Image'] | //input[@value='अपलोड इमेज']")
    public QAFWebElement subTabUploadImage;
	
	@FindBy(locator = "xpath=//input[@value='Bonafide Linking With I\\O Lettering'] | //input[@value='बोनाफाईड आणि पत्र व्यवहर लिंक करा']")
    public QAFWebElement subTabBonafideLinkingWithIOLettering;
	
	@FindBy(locator = "xpath=//input[@value='TC Linking With I\\O Lettering'] | //input[@value='टीसी आणि पत्र व्यवहार लिंक करा ']")
    public QAFWebElement subTabTCLinkingWithIOLettering;
	
	@FindBy(locator = "xpath=//input[@value='Student Name Setting']")
    public QAFWebElement subTabStudentNameSetting;
	
	
	//5th Sub-Menu Enquiry Locators
	@FindBy(locator = "xpath=//input[@value='Advertisement Type'] | //input[@value='जाहिरातीचा प्रकार']")
    public QAFWebElement subTabAdvertisementType;
	
	@FindBy(locator = "xpath=//input[@value='Class For Enquiry '] | //input[@value='चौकशी वर्ग']")
    public QAFWebElement subTabClassForEnquiry;
	
	
	//6th Sub-Menu Export/Import Data For Provisional Admission Locators
	
	
	//7th Sub-Menu Extra Curricular Activity Locators
	
	
	//8th Sub-Menu Class Management Locators
	
	
	//9th Sub-Menu Subject Section Locators
	
	
	//10th Sub-Menu Transport Setting Locators
	
	
	//11th Sub-Menu Challan Setting Locators
	
	
	//12th Sub-Menu Student Documents Locators

	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stubs
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForPageToLoad();
		super.waitForAjaxToComplete();
		try {
			tabAdmissionAndProvisionalAdmission.waitForVisible();
			tabAdmissionAndProvisionalAdmission.waitForPresent();
			
			tabUserAuthentication.waitForVisible();
			tabUserAuthentication.waitForPresent();

			tabStudentDocuments.waitForVisible();
			tabStudentDocuments.waitForPresent();

			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	   
	}
	
}
