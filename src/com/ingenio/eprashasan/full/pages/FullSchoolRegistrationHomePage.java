package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullSchoolRegistrationHomePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	
	@FindBy(locator = "xpath=//span[text()='Welcome to e-PRASHASAN']")
    public QAFWebElement headerWelcometoEPRASHASAN;
	
	
	@FindBy(locator = "xpath=//input[@name='schoolRegName']")
    public QAFWebElement textboxSchoolName;
	
	
	@FindBy(locator = "xpath=//input[@name='schoolSaunsthaName']")
    public QAFWebElement textboxSansthaName;
	
	
	@FindBy(locator = "xpath=//input[@name='SansthaRegNo']")
    public QAFWebElement textboxSansthaRegNo;
	
	
	@FindBy(locator = "xpath=//textarea[@id='schooladdr']")
    public QAFWebElement textareaSchoolAddress;
	
	
	@FindBy(locator = "xpath=//input[@name='regId']")
    public QAFWebElement textboxSchoolRegNo;
	
	
	@FindBy(locator = "xpath=//select[@name='schoolType']")
    public DropdownListWebElement dropdownSchoolType;
	
	
	@FindBy(locator = "xpath=//select[@name='schoolboard']")
    public DropdownListWebElement dropdownSchoolBoard;
	
	
	@FindBy(locator = "xpath=//input[@name='SansthaKey']")
    public QAFWebElement textboxSansthaKey;
	
	
	@FindBy(locator = "xpath=//input[@name='schoolkey']")
    public QAFWebElement textboxSchoolKey;
	
	
	@FindBy(locator = "xpath=//select[@name='language']")
    public DropdownListWebElement dropdownLanguage;
	


	@FindBy(locator = "xpath=//input[@name='firstName123']")
    public QAFWebElement textboxFirstName;
	
	
	@FindBy(locator = "xpath=//input[@name='lastName123']")
    public QAFWebElement textboxLastName;
	
	
	@FindBy(locator = "xpath=//input[@id='myUserNameId'] ")
    public QAFWebElement textboxUserName;
	
	
	@FindBy(locator = "xpath=//input[@id='passwordId']")
    public QAFWebElement textboxPassword;
	
	
	@FindBy(locator = "xpath=//select[@id='Question1']")
    public DropdownListWebElement dropdownQuestion1;

	
	@FindBy(locator = "xpath=//input[@id='answerId1']")
    public QAFWebElement textboxAnswer1;
	
	
	@FindBy(locator = "xpath=//select[@id='Question2']")
    public DropdownListWebElement dropdownQuestion2;
	
	
	@FindBy(locator = "xpath=//input[@id='anserId2']")
    public QAFWebElement textboxAnswer2;
	
	
	@FindBy(locator = "xpath=//input[@value='Submit']")
    public QAFWebElement buttonSubmit;
	
	@FindBy(locator = "xpath=//u[text()='Already Registered']")
    public QAFWebElement linkAlreadyRegistered;
	
	
	
	


	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
	}

	public void invoke(String extendedURL) {
		driver.manage().window().maximize();
		if(extendedURL!=null) {
			driver.get("/"+extendedURL);
		}
		else
		{
			driver.get("/");
		}
	}

	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		//super.waitForPageToLoad();
	//	super.waitForAjaxToComplete();
		try {
			headerWelcometoEPRASHASAN.waitForVisible();
			headerWelcometoEPRASHASAN.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}

	}
}
