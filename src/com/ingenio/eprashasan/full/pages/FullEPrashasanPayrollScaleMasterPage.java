package com.ingenio.eprashasan.full.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollScaleMasterPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//input[@id=\"scale\"]")
	public QAFWebElement textboxScale;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id=\"deletebtn\"]")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//input[@id=\"reset\"]")
	public QAFWebElement buttonReset;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
