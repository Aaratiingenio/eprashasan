package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "xpath=//a[contains(.,'Account Login')]")
	public TopNavigationComponent linkAccountLogin;
	
	@FindBy(locator = "xpath=//a[contains(.,'Create School College Registration')]")
	public TopNavigationComponent linkCreateSchoolCollegeRegistration;
	
	@FindBy(locator = "xpath=//a[3][contains(.,'Change Password')]")
	public TopNavigationComponent linkChangePassword;
	
	@FindBy(locator = "xpath=//a[contains(.,'Link Old Fees')]")
	public TopNavigationComponent linkOldFees;
	
	@FindBy(locator = "xpath=//a[text()=\"Voucher Entry\"]/child::span")
	public TopNavigationComponent linkVoucherEntry;
	
	@FindBy(locator = "xpath=//a[text()=\"Voucher Entry1\"]")
	public TopNavigationComponent linkVoucherEntry1;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Ledgers\")]")
	public TopNavigationComponent linkLedgers;
	
	@FindBy(locator = "xpath=//ul[@class=\"dropdown-menu\"]//a[contains(text(),\"Ledger Head\")]")
	public TopNavigationComponent linkLedgerHead;
	
	@FindBy(locator = "xpath=//ul[@class=\"dropdown-menu\"]//a[contains(text(),\"Ledger Sub Head\")]")
	public TopNavigationComponent linkLedgerSubHead;
	
	@FindBy(locator = "xpath=//a[contains(text(),\"Groups\")]/parent::li")
	public TopNavigationComponent linkGroups;
	
	@FindBy(locator = "xpath=//a[contains(text(),\"Bank Reconciliation\")]")
	public TopNavigationComponent linkBankReconciliation;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Account Statement\")]/child::span")
	public TopNavigationComponent linkAccountStatement;
	
	@FindBy(locator = "xpath=//ul[@class=\"dropdown-menu\"]//a[contains(text(),\"Account Statement\")]")
	public TopNavigationComponent linkAccountStatement1;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Receipt Payment Report\"]")
	public TopNavigationComponent linkReceiptPaymentReport;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Receipt Payment Head Report\")]")
	public TopNavigationComponent linkReceiptPaymentHeadReport;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Receipt Payment Report Date Wise\")]")
	public TopNavigationComponent linkReceiptPaymentReportDatewise;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Miscellaneous Setting\"]")
	public TopNavigationComponent linkMiscellaneousSetting;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Back Dated Entries\"]")
	public TopNavigationComponent linkBackDatedEntries;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Logout\"]")
	public TopNavigationComponent linkLogout;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
