package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollSalaryRegisterPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"staffScoolType\"]")
    public DropdownListWebElement selectSchoolType;
	
	@FindBy(locator = "xpath=//select[@id=\"staffDepartment\"]")
    public DropdownListWebElement selectStaffDepartment;
	
	@FindBy(locator = "xpath=//select[@id=\"staffDesignation\"]")
    public DropdownListWebElement selectStaffDesignation;
	
	@FindBy(locator = "xpath=//select[@id=\"staffNameCombo\"]")
    public DropdownListWebElement selectStaffName;
	
	@FindBy(locator = "xpath=//select[@id=\"fromYearIdCombo\"]")
    public DropdownListWebElement selectFromYear;
	
	@FindBy(locator = "xpath=//select[@id=\"fromMonth\"]")
    public DropdownListWebElement selectFromMonth;
	
	@FindBy(locator = "xpath=//select[@id=\"toYearIdCombo\"]")
    public DropdownListWebElement selectToYear;
	
	@FindBy(locator = "xpath=//select[@id=\"toMonth\"]")
    public DropdownListWebElement selectToMonth;
	
	@FindBy(locator = "xpath=//input[@value=\"Search\"]")
    public QAFWebElement buttonSearch;
	
	@FindBy(locator = "xpath=//img[@onclick=\"printSalaryReport();\"]")
    public QAFWebElement imgPrint;

	@FindBy(locator = "xpath=//img[@onclick=\"fnExcelReport();\"]")
    public QAFWebElement imgExcel;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
