package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollLoanAdvanceDisbursementPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//input[@id=\"loanAdvanceDate\"]")
    public QAFWebElement textboxLoanDate;
	
	@FindBy(locator = "xpath=//input[@id=\"loanAmount\"]")
    public QAFWebElement textboxLoanAmount;
	
	@FindBy(locator = "xpath=//input[@id=\"installmentId1\"]")
    public QAFWebElement checkboxInstallments;
	
	@FindBy(locator = "xpath=//input[@id=\"noOfInstallment\"]")
    public QAFWebElement textboxNoOfInstallment;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id=\"deletebtn\"]")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//input[@id=\"reset\"]")
	public QAFWebElement buttonReset;
	
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
