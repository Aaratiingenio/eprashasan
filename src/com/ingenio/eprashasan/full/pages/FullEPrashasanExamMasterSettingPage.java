package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanExamMasterSettingPage  extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="xpath=")
	public QAFWebElement headerExamMasterSetting;
	
	@FindBy(locator="xpath=//a[text()='Admin Setting']")
	public QAFWebElement linkAdminSetting;
	
	@FindBy(locator="xpath=//a[text()='Subject Setting']")
	public QAFWebElement linkSubjectSetting;
	
	@FindBy(locator="xpath=//a[text()='Add Batch']")
	public QAFWebElement linkAddBatch;
	
	
	
	
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForAjaxToComplete();
		try {
			headerExamMasterSetting.waitForVisible();
			headerExamMasterSetting.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  }




	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
}
	
	

