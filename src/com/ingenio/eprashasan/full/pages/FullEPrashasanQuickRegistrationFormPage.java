package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.components.BasicDetailsComponent;
//import com.ingenio.eprashasan.components.AssignFeeComponent;
import com.ingenio.eprashasan.components.OfficeDetailsComponent;
import com.ingenio.eprashasan.components.ParentDetailsComponent;
import com.ingenio.eprashasan.components.PreviousSchoolDetailsComponent;
import com.ingenio.eprashasan.components.RenewStudentComponent;
import com.ingenio.eprashasan.components.SearchStudentComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanQuickRegistrationFormPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "xpath=//h4[text()='QUICK REGISTRATION FORM']")
    public QAFWebElement headerQuickRegistrationForm;
	
	@FindBy(locator = "xpath=//legend[contains(.,'Office')]/parent::fieldset | //legend[contains(.,'à¤•à¤¾à¤°à¥�à¤¯à¤¾à¤²à¤¯')]/parent::fieldset")
    public OfficeDetailsComponent officeDetailsComponent;
//	
//	@FindBy(locator = "xpath=//legend[contains(.,'Assign Fee')]/parent::fieldset")
//    public AssignFeeComponent assignFeeComponent;
	
	@FindBy(locator = "xpath=//h4[contains(.,'Renew Student')]/ancestor::div[@class='modal-content']")
    public RenewStudentComponent renewStudentComponent;
	
	@FindBy(locator = "xpath=//input[@id='hideAndSeekSubmitId']")
    public QAFWebElement buttonAdd;
	 
	@FindBy(locator = "xpath=//input[@id='resetId']")
    public QAFWebElement buttonReset;
	
	@FindBy(locator = "xpath=//u[text()='Here']")
    public QAFWebElement linkForEditClickHere;
	
	@FindBy(locator = "xpath=//legend[contains(.,' Search Student')]/parent::fieldset")
	public SearchStudentComponent searchStudentComponent;
	
	@FindBy(locator = "xpath=//input[@onclick='reloadGridforfastpagination()']")
    public QAFWebElement buttonSearch;
	
	@FindBy(locator = "xpath=//legend[contains(normalize-space(),'Parent Details')]/parent::fieldset")
	public ParentDetailsComponent parentDetailsComponent;
	
	@FindBy(locator = "xpath=//legend[contains(normalize-space(),'Previous School Details')]/parent::fieldset")
	public PreviousSchoolDetailsComponent previousSchoolDetailsComponent;
	
	@FindBy(locator = "xpath=//legend[contains(normalize-space(),'Basic Details')]/parent::fieldset")
	public BasicDetailsComponent basicDetailsComponent;
	
	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		//super.waitForAjaxToComplete();
		try {
			headerQuickRegistrationForm.waitForVisible();
			headerQuickRegistrationForm.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	   
	}
}
