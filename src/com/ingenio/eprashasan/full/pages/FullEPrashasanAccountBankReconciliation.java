package com.ingenio.eprashasan.full.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountBankReconciliation extends WebDriverBaseTestPage<WebDriverTestPage>  {
	
	@FindBy(locator = "xpath=//a[@class=\"active\"]")
	public QAFWebElement tabBankName;
	
	@FindBy(locator = "xpath=//img[@src=\"/ePrashasanE1/resources/account/img/print.png\"]")
	public QAFWebElement printButton;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
