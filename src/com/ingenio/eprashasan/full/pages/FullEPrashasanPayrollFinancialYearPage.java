package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollFinancialYearPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"staffScoolType\"]")
	public DropdownListWebElement selectSchoolType;
	
	@FindBy(locator = "xpath=//select[@id=\"staffDepartment\"]")
	public DropdownListWebElement selectStaffDepartment;
	
	@FindBy(locator = "xpath=//select[@id=\"staffNameCombo\"]")
	public DropdownListWebElement selectStaffName;
	
	@FindBy(locator = "xpath=//select[@id=\"yearcombo\"]")
	public DropdownListWebElement selectAcademicYear;
	
	@FindBy(locator = "xpath=//select[@id=\"fyStartingMonth\"]")
	public DropdownListWebElement selectStartingMonth;
	
	@FindBy(locator = "xpath=//select[@id=\"fyEndingMonth\"]")
	public DropdownListWebElement selectEndingMonth;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id=\"deletebtn\"]")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//input[@id=\"reset\"]")
	public QAFWebElement buttonReset;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
