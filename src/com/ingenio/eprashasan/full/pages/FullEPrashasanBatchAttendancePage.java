package com.ingenio.eprashasan.full.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.ingenio.eprashasan.core.TodayDate;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

import jxl.write.DateTime;

public class FullEPrashasanBatchAttendancePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	@FindBy(locator= "xpath=//h4[.='Batch Attendance']")
	public QAFWebElement headerBatchAttendance;

	@FindBy(locator= "xpath=//select[@id='yearcombo']")
	public DropdownListWebElement dropdownAcademicYear;
	
	@FindBy(locator= "xpath=//select[@id='courseCombo']")
	public DropdownListWebElement dropdownCourse;
	
	@FindBy(locator= "xpath=//div[@id='showStandardButtons']")
    public QAFWebElement tabBatch;
	
	@FindBy(locator= "xpath=(//span[@class='tablink'])[1]")
    public QAFWebElement linkBatch1;
	
	@FindBy(locator= "xpath=(//span[@class='tablink'])[2]")
    public QAFWebElement linkBatch2;
	
	@FindBy(locator= "xpath=//input[@id='addSessionButton']")
	public QAFWebElement buttonAddSession;
	
	@FindBy(locator="xpath=//div[@style='padding: 0px;']")
	public PopupAddSession popupAddsession;
	
	@FindBy(locator="xpath=//fieldset[.//span[@id='headerTitle']]")
	public PopupMarkBatchWiseAttendance popupMarkBatchWiseAttendance;
	
	@FindBy(locator= "xpath=//u[text()='Mark Attendance']")
    public QAFWebElement linkMarkAttendance;
	
	
	
	
	public class PopupAddSession extends QAFWebComponent {
	
		@FindBy(locator= "xpath=//span[.='Add Session']")
		public QAFWebElement headerAddSession;
		
		@FindBy(locator= "xpath=//input[@id='sessionName']")
	    public QAFWebElement textboxSessionName;

		@FindBy(locator= "xpath=//select[@id='month']")
	    public DropdownListWebElement dropdownMonth;
		
		@FindBy(locator= "xpath=//select[@id='year']")
	    public DropdownListWebElement dropdownYear;
		
		@FindBy(locator= "xpath=(//td[@class='dates'])[7]")
	    public  QAFWebElement buttonDate;
		
		@FindBy(locator= "xpath=//input[@id='inTime']")
	    public QAFWebElement textboxInTime;
		
		@FindBy(locator= "xpath=//input[@id='outTime']")
	    public QAFWebElement textboxOutTime;
		
		@FindBy(locator= "xpath=//*[@id='submit']//following-sibling::input")
	    public QAFWebElement buttonSubmit;
		
		@FindBy(locator= "xpath=//*[@id='close']//following-sibling::input")
	    public QAFWebElement buttonClose;
		

	
		public PopupAddSession(QAFExtendedWebElement parent, String locator) {
		super(parent, locator);
			// TODO Auto-generated constructor stub
		}
		public PopupAddSession(String locator) {
			super(locator);
			// TODO Auto-generated constructor stub
		}
		public void waitForPopupToLoad() {
			try {
				headerAddSession.waitForPresent();
				headerAddSession.waitForVisible();
				
				Reporter.log(this.getClass().getSimpleName() + " Popup loaded Sucessfully");
			}catch(Exception e) {
				Reporter.log(this.getClass().getSimpleName() + " Popup Did not Load");
			}
		}
	}
			
		public class PopupMarkBatchWiseAttendance extends QAFWebComponent{
			
			@FindBy(locator= "xpath=//span[text()='Mark Batchwise Attendance']")
			public QAFWebElement headerMarkBatchWiseAttedance;
			
			@FindBy(locator="xpath=//button[@id='greenBox']")
			public QAFWebElement buttonPresent;
			
			@FindBy(locator="xpath=//button[@id='redBox']")
			public QAFWebElement buttonAbsent;
			
			@FindBy(locator="xpath=(//span[@class='absentPresentDot'])[1]")
			public QAFWebElement labelPresentAbsent;
			
			@FindBy(locator="xpath=//input[@id='submitAttendance']")
			public QAFWebElement buttonSubmit;
			
			@FindBy(locator="xpath=//input[@id='deleteAttendance']")
			public QAFWebElement buttonDelete;
			
			@FindBy(locator="xpath=//input[@value='Close']")
			public QAFWebElement buttonClose;
			
			public PopupMarkBatchWiseAttendance(QAFExtendedWebElement parent, String locator) {
				super(parent, locator);
					// TODO Auto-generated constructor stub
				}
				public PopupMarkBatchWiseAttendance(String locator) {
					super(locator);
					// TODO Auto-generated constructor stub
				}
				public void waitForPopupToLoad() {
					try {
						headerMarkBatchWiseAttedance.waitForPresent();
						headerMarkBatchWiseAttedance.waitForVisible();
						
						Reporter.log(this.getClass().getSimpleName() + " Popup loaded Sucessfully");
					}catch(Exception e) {
						Reporter.log(this.getClass().getSimpleName() + " Popup Did not Load");
					}
				}
			}

		@Override
		protected void openPage(PageLocator locator, Object... args) {
			// TODO Auto-generated method stub
		}
	
		}
			
			
	
			
	