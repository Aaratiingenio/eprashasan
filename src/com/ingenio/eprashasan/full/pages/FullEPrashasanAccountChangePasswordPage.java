package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountChangePasswordPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"schoolName\"]")
	public DropdownListWebElement selectSchoolName;
	
	@FindBy(locator = "xpath=//input[@id=\"userName\"]")
	public QAFWebElement textboxUserName;
	
	@FindBy(locator = "xpath=//input[@id=\"userPassword\"]")
	public QAFWebElement textboxPassword;
	
	@FindBy(locator = "xpath=//input[@id=\"newpassword\"]")
	public QAFWebElement textboxNewPassword;
	
	@FindBy(locator = "xpath=//input[@value=\"Login\"]")
	public QAFWebElement buttonlogin;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
