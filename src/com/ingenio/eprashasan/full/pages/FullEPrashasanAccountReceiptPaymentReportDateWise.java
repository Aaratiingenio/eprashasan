package com.ingenio.eprashasan.full.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountReceiptPaymentReportDateWise extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//input[@id=\"fromDateId\"]")
	public QAFWebElement textboxFromDate;
	
	@FindBy(locator = "xpath=//input[@id=\"toDateId\"]")
	public QAFWebElement textboxToDate;
	
	@FindBy(locator = "xpath=//input[@class=\"myButton\"]")
	public QAFWebElement buttonSearch;
	
	@FindBy(locator = "xpath=//img[@src=\"/ePrashasanE1/resources/account/img/print.png\"]")
	public QAFWebElement buttonPrint;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
