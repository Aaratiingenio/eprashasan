package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollGenerateSalaryPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "xpath=//input[@id=\"salaryProcessDate\"]")
	public QAFWebElement textboxSalaryProcessDate;
	
	@FindBy(locator = "xpath=//select[@id=\"staffNameCombo\"]")
	public DropdownListWebElement selectStaffName;
	
	@FindBy(locator = "xpath=//select[@id=\"monthcomboID\"]")
	public DropdownListWebElement selectMonth;
	
	@FindBy(locator = "xpath=//select[@id=\"yearcomboID\"]")
	public DropdownListWebElement selectYear;
	
	@FindBy(locator = "xpath=//div[@id=\"uniform-attendanceBy\"]")
	public QAFWebElement checkboxAttendanceByManual;
	
	@FindBy(locator = "xpath=//select[@id=\"payTypeCombo\"]")
	public DropdownListWebElement selectPayType;
	
	@FindBy(locator = "xpath=//input[@id=\"ddchequeId\"]")
	public QAFWebElement textboxDDChequeChallanNo;
	
	@FindBy(locator = "xpath=//select[@id=\"bankName\"]")
	public DropdownListWebElement selectBankName;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

	
	

}
