package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollProcessSalaryStatusPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"month\"]")
    public DropdownListWebElement selectMonthName;
	
	@FindBy(locator = "xpath=//select[@id=\"yearIdCombo\"]")
    public DropdownListWebElement selectYear;
	
	@FindBy(locator = "xpath=//input[@id=\"search\"]")
    public QAFWebElement buttonSearch;
	
	@FindBy(locator = "xpath=//span[@class=\"myButtonRed\"]")
    public QAFWebElement buttonSalaryStatus;
	
	@FindBy(locator = "xpath=//a[@id=\"viewSalarySlipPreview\"]")
    public QAFWebElement buttonSalaryPreview;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

	
}
