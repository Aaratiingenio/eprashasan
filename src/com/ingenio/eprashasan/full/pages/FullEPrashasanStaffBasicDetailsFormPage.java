package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.components.BasicDetailsComponent;

import com.ingenio.eprashasan.components.OfficeDetailsComponent;
import com.ingenio.eprashasan.components.StudentContactDetailsComponent;
import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.ingenio.eprashasan.components.AddressDetailsComponent.LocalAddressDetailsComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanStaffBasicDetailsFormPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator = "xpath=//h4[text()='Staff Basic Details']")
	public QAFWebElement headerStaffBasicDetailsForm;
	
	@FindBy(locator = "xpath=//ul[@class='top-level-menu']")
	public TopNavigationComponent topNavigationComponent;
	
	@FindBy(locator = "xpath=//legend[contains(.,'Office Details')]/parent::fieldset")
	public OfficeDetailsComponent officeDetailsComponent;
	
	@FindBy(locator = "xpath=//legend[contains(.,'Basic Details')]/parent::fieldset")
	public BasicDetailsComponent basicDetailsComponent;
	
	@FindBy(locator = "xpath=//legend[contains(.,'Staff Contact')]/parent::fieldset")
	public StudentContactDetailsComponent staffContactDetailsComponent;
	
	@FindBy(locator = "xpath=//b[starts-with(.,'Permanent address same')]")
    public QAFWebElement checkboxLabelPermanentAddressSameAsLocalAddress;
	
	@FindBy(locator = "xpath=//following::fieldset[contains(.,'Local')]")
    public LocalAddressDetailsComponent localAddressDetailsComponent;
	
	@FindBy(locator = "xpath=//input[@id='savebasicInfoId']")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id='staffresetId']")
	public QAFWebElement buttonReset;
	
	
	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForAjaxToComplete();
		try {
			headerStaffBasicDetailsForm.waitForVisible();
			headerStaffBasicDetailsForm.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  }
}
