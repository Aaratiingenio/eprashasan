package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollTDSSlabPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"yearId\"]")
	public DropdownListWebElement selectAcademicYear;
	
	@FindBy(locator = "xpath=//input[@id=\"effectiveFromDate\"]")
	public DropdownListWebElement selectEffectiveFromDate;

	@FindBy(locator = "xpath=//input[@id=\"incomeAmtStartRange\"]")
	public QAFWebElement textboxIncomeAmtStartRange;
	
	@FindBy(locator = "xpath=//input[@id=\"incomeAmtEndRange\"]")
	public QAFWebElement textboxIncomeAmtEndRange;
	
	@FindBy(locator = "xpath=//input[@id=\"tdsSlabPercentage\"]")
	public QAFWebElement textboxTdsSlabPercentage;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id=\"deletebtn\"]")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//input[@id=\"reset\"]")
	public QAFWebElement buttonReset;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
