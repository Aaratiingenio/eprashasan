/**
 * 
 */
package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

/**
 * @author admin
 *
 */
public class FullEPrashasanExcelImportPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator = "xpath=//h4[text()='EXCEL IMPORT']")
	public QAFWebElement headerExcelImport;
	
	@FindBy(locator = "xpath=//a[text()='Here']")
	public QAFWebElement linkExcelImport;
	

	@FindBy(locator = "xpath=//input[@value='Choose File']")
	public QAFWebElement buttonChooseFile;
	

	@FindBy(locator = "xpath=//input[@id='SaveExcel']")
	public QAFWebElement buttonCheckExcel;
	

	@FindBy(locator = "xpath=//input[@value='Save Excel']")
	public QAFWebElement buttonSaveExcel;
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForAjaxToComplete();
		try {
			headerExcelImport.waitForVisible();
			headerExcelImport.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  }
}
