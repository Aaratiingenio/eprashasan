/**
 * 
 */
package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

/**
 * @author admin
 *
 */
public class FullEPrashasanStaffRelativeOccupationPage extends WebDriverBaseTestPage<WebDriverTestPage>  {

	@FindBy(locator = "xpath=//h4[contains(text(),'Staff Relative Occupation')]")
    public QAFWebElement headerStaffRelativeOccupation;
	
	@FindBy(locator = "xpath=//input[@id='occupationNameId']")
    public QAFWebElement textboxStaffRelativeOccupation;
	
	@FindBy(locator = "xpath=//input[@id='saveRelativeOccupationID']")
    public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id='updateStaffRelationID']")
    public QAFWebElement buttonUpdate;
	
	@FindBy(locator = "xpath=//input[@id='reset']")
    public QAFWebElement buttonReset;
	
	@FindBy(locator = "xpath=//div[text()='Delete']")
    public QAFWebElement buttonDelete;

	@FindBy(locator = "xpath=//td[text()='2']")
    public QAFWebElement tablerow2;
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
//	super.waitForAjaxToComplete();
		try {
			headerStaffRelativeOccupation.waitForVisible();
			headerStaffRelativeOccupation.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  } 
}