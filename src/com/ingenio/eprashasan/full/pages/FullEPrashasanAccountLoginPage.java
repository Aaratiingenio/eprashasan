package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanAccountLoginPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	/*@FindBy(locator = "xpath=//h4[contains(text(),\"Account Login\")]")
	public QAFWebElement headerAccountLogin;*/
	
	@FindBy(locator = "xpath=//select[@id=\"schoolName\"]")
	public DropdownListWebElement selectSchoolName;
	
	@FindBy(locator = "xpath=//input[@id=\"userName\"]")
	public QAFWebElement textboxUsername;
	
	@FindBy(locator = "xpath=//input[@id=\"userPassword\"]")
	public QAFWebElement textboxPassword;
	
	@FindBy(locator = "xpath=//input[@value=\"Login\"]")
	public QAFWebElement buttonLogin;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
}
