package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountMiscellaneousSetting extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//span[@id=\"Date1\"]")
	public QAFWebElement linkChooseDateOfClosing;
	
	@FindBy(locator = "xpath=//select[@id=\"dd1\"]")
	public DropdownListWebElement selectDD;

	@FindBy(locator = "xpath=//select[@id=\"mm1\"]")
	public DropdownListWebElement selectMM;
	
	@FindBy(locator = "xpath=//select[@id=\"yyyy1\"]")
	public DropdownListWebElement selectYY;
	
	@FindBy(locator = "xpath=//b[contains(.,\"Close Financial Year :\")]")
	public QAFWebElement linkCloseFinancialYear;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
