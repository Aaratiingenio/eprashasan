package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountStatement extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "xpath=//a[contains(.,\"Day Book\")]")
	public QAFWebElement tabDayBook;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Depreciation Annexure\")]")
	public QAFWebElement tabDepreciationAnnexure;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Ledger Summary/Trial Balance\")]")
	public QAFWebElement tabLedgerSummaryTrialBalance;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Income & Expenditure Statement\")]")
	public QAFWebElement tabIncomeExpenditureStatement;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Balance Sheet\")]")
	public QAFWebElement tabBalanceSheet;
	
	@FindBy(locator = "xpath=//a[contains(.,\"70% and 30%\")]")
	public QAFWebElement tab7030;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
