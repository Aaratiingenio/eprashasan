package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountLedgerSubHead extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//img[@src=\"/ePrashasanE1/resources/account/img/add.png\"]")
	public QAFWebElement linkCreateNewLedger;
	
	@FindBy(locator = "xpath=//select[@id=\"ledgerHeadID\"]")
	public DropdownListWebElement selectLedgerHead;
	
	@FindBy(locator = "xpath=//textarea[@id=\"ledgerName\"]")
	public QAFWebElement textareaSubLedger;
	
	@FindBy(locator = "xpath=//select[@id=\"getGroupSubgroup1\"]")
	public DropdownListWebElement selectGroup;
	
	@FindBy(locator = "xpath=//input[@id=\"openingBalanceAmt\"]")
	public QAFWebElement textboxOpeningBalance;
	
	@FindBy(locator = "xpath=//select[@id=\"amountType\"]")
	public DropdownListWebElement selectAmountType;
	
	@FindBy(locator = "xpath=//input[@id=\"depreciation\"]")
	public QAFWebElement textboxDepreciation;
	
	@FindBy(locator = "xpath=//select[@id=\"seventyThirty\"]")
	public DropdownListWebElement select7030;
	
	@FindBy(locator = "xpath=(//img[@src=\"/ePrashasanE1/resources/account/img/edit.png\"])[1]")
	public QAFWebElement imgEdit;
	
	@FindBy(locator = "xpath=((//img[@src=\"/ePrashasanE1/resources/account/img/delete.png\"])[1]")
	public QAFWebElement imgDelete;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
