package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountVoucherEntry1Page extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//a[contains(.,\"Voucher Entry\")]/parent::b")
	public QAFWebElement headerVoucherEntry;
	
	@FindBy(locator = "xpath=//img[@src=\"/ePrashasanE1/resources/account/img/add.png\"]")
	public QAFWebElement imgNewEntry;
	
	@FindBy(locator = "xpath=//select[@id=\"dd\"]")
	public DropdownListWebElement selectDate;
	
	@FindBy(locator = "xpath=//select[@id=\"mm\"]")
	public DropdownListWebElement selectMonth;
	
	@FindBy(locator = "xpath=//select[@id=\"yyyy\"]")
	public DropdownListWebElement selectYear;
	
	@FindBy(locator = "xpath=//select[@id=\"vType\"]")
	public DropdownListWebElement selectVoucherType;
	
	@FindBy(locator = "xpath=//select[@id=\"debitLedger\"]")
	public DropdownListWebElement selectDebitLedger;
	
	@FindBy(locator = "xpath=//select[@id=\"creditLedger\"]")
	public DropdownListWebElement selectCreditLedger;
	
	@FindBy(locator = "xpath=//input[@id=\"narration\"]")
	public QAFWebElement textboxNarration;
	
	@FindBy(locator = "xpath=//input[@id=\"pDetails\"]")
	public QAFWebElement textboxPaymentDetails;
	
	@FindBy(locator = "xpath=//input[@id=\"debit\"]")
	public QAFWebElement textboxAmount;
	
	@FindBy(locator = "xpath=(//img[@alt=\"Print\"])[1]")
	public QAFWebElement imgPrint;
	
	@FindBy(locator = "xpath=((//img[@alt=\"Edite\"])[1]")
	public QAFWebElement imgEdit;
	
	@FindBy(locator = "xpath=((//img[@alt=\"Delete\"])[1]")
	public QAFWebElement imgDelete;
	
	@FindBy(locator = "xpath=(//img[@src=\"/ePrashasanE1/resources/account/img/copy.png\"])[1]")
	public QAFWebElement imgCopy;
	
	
	
	
	
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
