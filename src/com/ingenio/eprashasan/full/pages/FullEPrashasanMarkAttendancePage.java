package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanMarkAttendancePage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator="xpath=//h4[.='MARK ATTENDANCE']")
	public QAFWebElement headerMarkAttendance;
	
	@FindBy(locator = "xpath=//ul[contains(@class,'subMenuAlign')]")
	public TopNavigationComponent topNavigationComponent;
	
	@FindBy(locator="xpath=//a[.='Attendance']")
	public QAFWebElement linkAttendance;
		
	@FindBy(locator="xpath=//a[.='Batch-Wise Attendance']")
	public QAFWebElement linkBatchWiseAttendance;
	
	
	@Override
	
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForAjaxToComplete();
		try {
			headerMarkAttendance.waitForVisible();
			headerMarkAttendance.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  
	}


}
