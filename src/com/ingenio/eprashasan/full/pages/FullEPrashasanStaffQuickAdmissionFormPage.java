/**
 * 
 */
package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

/**
 * @author admin
 *
 */
public class FullEPrashasanStaffQuickAdmissionFormPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator = "xpath=//h4[contains(.,'Staff Quick Admission')]")
	public QAFWebElement headerStaffQuickAdmission;
	
	@FindBy(locator = "xpath=//select[@id='staffRoleId_0']")
	public DropdownListWebElement dropdownStaffRole;
	
	@FindBy(locator = "xpath=//select[@id='initialNameId_0']")
	public DropdownListWebElement dropdownInitialName;
	
	@FindBy(locator = "xpath=//input[@id='firstNameId_0']")
	public QAFWebElement textboxFirstName;
	
	@FindBy(locator = "xpath=//input[@id='middleNameId_0']")
	public QAFWebElement textboxMiddleName;
	
	@FindBy(locator = "xpath=//input[@id='lastNameId_0']")
	public QAFWebElement textboxLastName;
	
	@FindBy(locator = "xpath=//input[@id='mobileNumberId_0']")
	public QAFWebElement textboxMobileNo;
	
	@FindBy(locator = "xpath=//u[text()='here']")
	public QAFWebElement linkEditHere;
	
	@FindBy(locator = "xpath=//input[@id='addButton']")
	public QAFWebElement buttonAddMore;

	@FindBy(locator = "xpath=//input[@id='saveButton']")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id='staffresetId']")
	public QAFWebElement buttonReset;
	
	@FindBy(locator = "xpath=//input[@id='updateButton']")
	public QAFWebElement buttonUpdate;
	
   @FindBy(locator = "xpath=//div[text()='[EDIT]']")
	public QAFWebElement buttonEdit;
	
    @FindBy(locator = "xpath=//div[text()='[DELETE]']")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//select[@id='staffRoleId_1']")
	public DropdownListWebElement dropdownStaffRole1;
	
	@FindBy(locator = "xpath=//select[@id='initialNameId_1']")
	public DropdownListWebElement dropdownInitialName1;
	
	@FindBy(locator = "xpath=//input[@id='firstNameId_1']")
	public QAFWebElement textboxFirstName1;
	
	@FindBy(locator = "xpath=//input[@id='middleNameId_1']")
	public QAFWebElement textboxMiddleName1;
	
	@FindBy(locator = "xpath=//input[@id='lastNameId_1']")
	public QAFWebElement textboxLastName1;
	
	@FindBy(locator = "xpath=//input[@id='mobileNumberId_1']")
	public QAFWebElement textboxMobileNo1;
	
	@FindBy(locator = "xpath=//a[text()='Remove']")
	public QAFWebElement linkRemove;
	
	@FindBy(locator = "xpath=//td[text()='1']")
	public QAFWebElement textfieldRow;
	
    @Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForAjaxToComplete();
		try {
			headerStaffQuickAdmission.waitForVisible();
			headerStaffQuickAdmission.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  }

	
	

}
