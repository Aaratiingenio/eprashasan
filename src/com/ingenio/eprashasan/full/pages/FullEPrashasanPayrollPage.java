package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;

public class FullEPrashasanPayrollPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Setting\"]")
	public TopNavigationComponent linkSetting;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Sandwich Leave, Half Day And Attendance Manual Or Automatic Setting\")]")
	public TopNavigationComponent linkSandwichHalfAutomatics;

	@FindBy(locator = "xpath=//a[contains(.,\"Salary Setting\")]")
	public TopNavigationComponent linkSalarySetting;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Financial Year\")]")
	public TopNavigationComponent linkFinancialYear;
	
	@FindBy(locator = "xpath=(//a[text()=\"Master\"])[1]")
	public TopNavigationComponent linkSMaster;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Grade\")]")
	public TopNavigationComponent linkMasterGrade;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Scale\")]")
	public TopNavigationComponent linkMasterScale;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Allowances\")]")
	public TopNavigationComponent linkAllowances;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Allowance Make For Reports\")]")
	public TopNavigationComponent linkAllowanceMakeForReports;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Actual Allowance\")]")
	public TopNavigationComponent linkActualAllowance;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Deduction\"]")
	public TopNavigationComponent linkDeduction;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Deduction Make For Reports\"]")
	public TopNavigationComponent linkDeductionMakeForReports;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Actual Deduction\"]")
	public TopNavigationComponent linkActualDeduction;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"TDS Slab\"]")
	public TopNavigationComponent linkTDSSlab;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Salary Structure\"]")
	public TopNavigationComponent linkSalaryStructure;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Leave Setting\"]")
	public TopNavigationComponent linkLeaveSetting;
	
	@FindBy(locator = "xpath=(//a[normalize-space(.)=\"Master\"])[2]")
	public TopNavigationComponent linkLMaster;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Leave Category\"]")
	public TopNavigationComponent linkMasterLeaveCategory;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Leave\"]")
	public TopNavigationComponent linkMasterLeave;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Amount Deduction For Leave\"]")
	public TopNavigationComponent linkAmtDeductionForLeave;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Late Mark\"]")
	public TopNavigationComponent linkLateMark;
	
	@FindBy(locator = "xpath=//a[normalize-space(.)=\"Leave Structure\"]")
	public TopNavigationComponent linkLeaveStructure;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
