package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollSalaryBankStatementPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"month\"]")
    public DropdownListWebElement selectMonth;
	
	@FindBy(locator = "xpath=//select[@id=\"yearIdCombo\"]")
    public DropdownListWebElement selectYear;
	
	@FindBy(locator = "xpath=//select[@id=\"staffScoolType\"]")
    public DropdownListWebElement selectSchoolType;
	
	@FindBy(locator = "xpath=//select[@id=\"schoolBankName\"]")
    public DropdownListWebElement selectBankName;
	
	@FindBy(locator = "xpath=//input[@value=\"Search\"]")
    public QAFWebElement buttonSearch;
	
	@FindBy(locator = "xpath=//input[@id=\"l2\"]")
    public QAFWebElement checkboxStafRegNo;
	
	@FindBy(locator = "xpath=//input[@id=\"l4\"]")
    public QAFWebElement checkboxStaffAddress;
	
	@FindBy(locator = "xpath=//input[@id=\"l5\"]")
    public QAFWebElement checkboxBankName;
	
	@FindBy(locator = "xpath=//input[@id=\"l6\"]")
    public QAFWebElement checkboxBranchName;
	
	@FindBy(locator = "xpath=//input[@id=\"l8\"]")
    public QAFWebElement checkboxIFSCCode;
	
	@FindBy(locator = "xpath=//input[@id=\"l9\"]")
    public QAFWebElement checkboxMIRCode;
	
	@FindBy(locator = "xpath=//input[@id=\"sigField1\"]")
    public QAFWebElement textboxField1;
	
	@FindBy(locator = "xpath=//input[@id=\"sigField2\"]")
    public QAFWebElement textboxField2;
	
	@FindBy(locator = "xpath=//input[@id=\"sigField3\"]")
    public QAFWebElement textboxField3;
	
	@FindBy(locator = "xpath=//input[@id=\"sigField4\"]")
    public QAFWebElement textboxField4;
	
	@FindBy(locator = "xpath=//input[@id=\"sigField5\"]")
    public QAFWebElement textboxField5;
	
	@FindBy(locator = "xpath=//a[@id=\"generatePrint\"]")
    public QAFWebElement buttonPrintPreview;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
