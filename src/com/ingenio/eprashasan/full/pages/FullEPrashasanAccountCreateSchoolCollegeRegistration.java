package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountCreateSchoolCollegeRegistration extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "xpath=//select[@id=\"orgTypeID\"]")
	public DropdownListWebElement selectOrgType;
	
	@FindBy(locator = "xpath=//input[@id=\"schoolName\"]")
	public QAFWebElement textboxSchoolName;
	
	@FindBy(locator = "xpath=//input[@id=\"schoolRegiNo\"]")
	public QAFWebElement textboxRegNo;
	
	@FindBy(locator = "xpath=//textarea[@id=\"addressAc\"]")
	public QAFWebElement textareaAddress;
	
	@FindBy(locator = "xpath=//input[@id=\"regiDate\"]")
	public QAFWebElement textboxRegDate ;
	
	@FindBy(locator = "xpath=//input[@id=\"panNo\"]")
	public QAFWebElement textboxPanNo;
	
	@FindBy(locator = "xpath=//input[@id=\"vatTinNo\"]")
	public QAFWebElement textboxVatNo;
	
	@FindBy(locator = "xpath=//input[@id=\"tanNo\"]")
	public QAFWebElement textboxTanNo;
	
	@FindBy(locator = "xpath=//input[@id=\"serviceTaxNo\"]")
	public QAFWebElement textboxServiceTaxNo;
	
	@FindBy(locator = "xpath=//input[@id=\"catNo\"]")
	public QAFWebElement textboxCstNo;
	
	@FindBy(locator = "xpath=//input[@id=\"userName\"]")
	public QAFWebElement textboxUsername;
	
	@FindBy(locator = "xpath=//input[@id=\"userPassword\"]")
	public QAFWebElement textboxPassword;
	
	@FindBy(locator = "xpath=//input[@id=\"rePassword\"]")
	public QAFWebElement textboxRetypePassword;
	
	@FindBy(locator = "xpath=//input[@value=\"add\"]")
	public QAFWebElement buttonAdd;
	
	@FindBy(locator = "xpath=(//img[@alt=\"Edite\"])[1]")
	public QAFWebElement imgEdit;
	
	@FindBy(locator = "xpath=(//img[@alt='Delete'])[4]")
	public QAFWebElement imgDelete;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
	

}
