/**
 * 
 */
package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

/**
 * @author admin
 *
 */
public class FullEPrashasanStaffRelationPage extends WebDriverBaseTestPage<WebDriverTestPage>  {

	@FindBy(locator = "xpath=//h4[contains(text(),'Staff Relation')]")
    public QAFWebElement headerStaffRelation;
	
	@FindBy(locator = "xpath=//input[@id='relationNameId']")
    public QAFWebElement textboxStaffRelation;
	
	@FindBy(locator = "xpath=//input[@id='saveStaffRelationID']")
    public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id='updateStaffRelationID']")
    public QAFWebElement buttonUpdate;
	
	@FindBy(locator = "xpath=//input[@id='reset']")
    public QAFWebElement buttonReset;
	
	@FindBy(locator = "xpath=//div[text()='Delete']")
    public QAFWebElement buttonDelete;

	@FindBy(locator = "xpath=//td[text()='3']")
    public QAFWebElement tablerow3;
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
//	super.waitForAjaxToComplete();
		try {
			headerStaffRelation.waitForVisible();
			headerStaffRelation.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  }


}
