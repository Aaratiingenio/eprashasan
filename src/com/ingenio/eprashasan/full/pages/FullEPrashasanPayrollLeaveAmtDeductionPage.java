package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollLeaveAmtDeductionPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//select[@id=\"yearCombo\"]")
	public DropdownListWebElement selectYear;
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
