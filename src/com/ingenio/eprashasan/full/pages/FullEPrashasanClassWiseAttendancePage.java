package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.components.TopNavigationComponent;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanClassWiseAttendancePage  {

	
	@FindBy(locator = "xpath=//h4[.='MARK ATTENDANCE']")
	public QAFWebElement headerMarkAttendance;
	
	@FindBy(locator="xpath=//select[@id='yearcombo']")
	public QAFWebElement dropdownAcademicYear;
	
	
	
	
}
