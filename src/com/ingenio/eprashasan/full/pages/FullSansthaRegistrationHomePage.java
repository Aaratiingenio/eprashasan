package com.ingenio.eprashasan.full.pages;

import org.testng.Assert;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullSansthaRegistrationHomePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//span[.='Welcome to e-PRASHASAN']")
    public QAFWebElement headerWelcometoEPRASHASAN;
	
	@FindBy(locator = "xpath=//input[@id='SansthaName']")
    public QAFWebElement textboxSansthaName;
	
	@FindBy(locator = "xpath=//input[@id='SansthaRegNo']")
    public QAFWebElement textboxSansthaRegNo;
	
	@FindBy(locator = "xpath=//input[@id='SansthaKey']")
    public QAFWebElement textboxSansthaKey;
	
	@FindBy(locator = "xpath=//input[@id='Website']")
    public QAFWebElement textboxWebsite;
	
	@FindBy(locator = "xpath=//input[@id='contactNo']")
    public QAFWebElement textboxContactNo;
	
	@FindBy(locator = "xpath=//input[@id='state']")
    public QAFWebElement textboxState;
	
	@FindBy(locator = "xpath=//input[@id='pincode']")
    public QAFWebElement textboxPincode;
	
	@FindBy(locator = "xpath=//input[@id='email']")
    public QAFWebElement textboxEmail;
	
	@FindBy(locator="xpath=//input[@id='faxNo']")
	public QAFWebElement textboxFax;
	
	@FindBy(locator="xpath=//textarea[@id='address']")
	public QAFWebElement textboxAddress;
	
	@FindBy(locator="xpath=//input[@id='city']")
	public QAFWebElement textboxCity;
	
	@FindBy(locator="xpath=//input[@id='firstNameUsernameId']")
	public QAFWebElement textboxFirstName;
	
	@FindBy(locator="xpath=//input[@id='lastnameId']")
	public QAFWebElement textboxLastName;
	
	@FindBy(locator="xpath=//input[@id='myUserNameId']")
	public QAFWebElement textboxmyUserNameId;
	
	@FindBy(locator="xpath=//input[@id='passwordId']")
	public QAFWebElement textboxpasswordId;
	
	@FindBy(locator="xpath=//select[@id='Question1']")
	public DropdownListWebElement dropdownboxQuestion1;
	
	@FindBy(locator="xpath=//input[@id='answerId1']")
	public QAFWebElement textboxanswer1;
	
	@FindBy(locator="xpath=//select[@id='Question2']")
	public DropdownListWebElement dropdownboxQuestion2;
	
	@FindBy(locator="xpath=//input[@id='anserId2']")
	public QAFWebElement textboxanswer2;
	
	@FindBy(locator="xpath=//input[@value='Submit']")
	public QAFWebElement buttonSubmit;
	
	@FindBy(locator="xpath=//u[.='Already Registered']")
	public QAFWebElement linkAlreadyRegistrerd;
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
	}

	public void invoke(String extendedURL) {
		driver.manage().window().maximize();
		if(extendedURL!=null) {
			driver.get("/"+extendedURL);
		}
	}

	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForPageToLoad();
		super.waitForAjaxToComplete();
		try {
			headerWelcometoEPRASHASAN.waitForVisible();
			headerWelcometoEPRASHASAN.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}

	}
}

