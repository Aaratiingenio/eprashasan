package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollLeaveStructurePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//input[@name=\"payrollLeaveCategoryIdArr\"]")
	public QAFWebElement checkboxLeaveCategory;
	
	@FindBy(locator = "xpath=//input[@name=\"leaveTypIdArr\"]")
	public QAFWebElement checkboxSelectRow;
	
	@FindBy(locator = "xpath=//input[@name=\"noOfLeavesArr\"]")
	public QAFWebElement textboxNoOfLeaves;
	
	@FindBy(locator = "xpath=//select[@name=\"monthlyYearlyArr\"]")
	public DropdownListWebElement selectMonthlyYearly;
	
	@FindBy(locator = "xpath= //input[contains(@id, \"leavePolicy\")]")
	public QAFWebElement radiobuttonLeavePolicy;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id=\"deletebtn\"]")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//input[@id=\"reset\"]")
	public QAFWebElement buttonReset;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
