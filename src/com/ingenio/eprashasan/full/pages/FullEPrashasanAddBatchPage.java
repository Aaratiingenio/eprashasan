package com.ingenio.eprashasan.full.pages;

import java.util.List;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.ingenio.eprashasan.core.DropdownListWebElement;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class FullEPrashasanAddBatchPage extends WebDriverBaseTestPage<WebDriverTestPage>  {

	@FindBy(locator="xpath=//b[text()='Add Batch']")
	public QAFWebElement headerAddBatch;
	
	@FindBy(locator="xpath=//select[@id='yearcombo']")
	public DropdownListWebElement dropdownAcademicYear;
	
	@FindBy(locator="xpath=//select[@id='courseCombo']")
	public DropdownListWebElement dropdownSelectCourse;
	
	@FindBy(locator="xpath=//button[text()=' Add Batch']")
	public QAFWebElement buttonAddBatch;
	
	@FindBy(locator="xpath=//fieldset[.//h4[text()='Add Batch']]")
	public PopupAddBatch popupAddBatch;

	@FindBy(locator="xpath=//a[text()='Assign Students']")
	public QAFWebElement linkAssignStudent;
	
	@FindBy(locator="xpath=//fieldset[.//h4[text()='Student List']]")
	public PopupStudentList popupStudentList;
	
	@FindBy(locator="xpath=//button[text()='Save']")
	public QAFWebElement buttonSave;
	
	@FindBy(locator="xpath=//button[text()=' Delete']")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator= "xpath=//div[@id='showStandardButtons']")
    public QAFWebElement tabBatch;
	
	@FindBy(locator= "xpath=//input[@id='batchName']")
    public QAFWebElement textboxBatchName;
	
	
	public class PopupAddBatch extends QAFWebComponent{
		
		@FindBy(locator="xpath=//h4[text()='Add Batch']")
		public QAFWebElement headerAddBatch;
		
		@FindBy(locator="xpath=//input[@id='newBatchName']")
		public QAFWebElement textboxBatchName;
		
		@FindBy(locator="xpath=//input[@id='newSelectall']")
		public QAFWebElement checkBoxSelectAll;
		
		@FindBy(locator="xpath=//button[text()=' Save Batch']")
		public QAFWebElement buttonSaveBatch;
		
		@FindBy(locator="xpath=//span[@id='total']")
		public QAFWebElement totalcount;
		
		@FindBy(locator="xpath=//input[@type='checkbox']")
		public List<WebElement> checkboxSelect;
	
		
		
		public PopupAddBatch(QAFExtendedWebElement parent, String locator) {
			super(parent, locator);
				// TODO Auto-generated constructor stub
			}
			public PopupAddBatch(String locator) {
				super(locator);
				// TODO Auto-generated constructor stub
			}
			public void waitForPopupToLoad() {
				try {
					headerAddBatch.waitForPresent();
					headerAddBatch.waitForVisible();
					
					Reporter.log(this.getClass().getSimpleName() + " Popup loaded Sucessfully");
				}catch(Exception e) {
					Reporter.log(this.getClass().getSimpleName() + " Popup Did not Load");
				}
			}
			
			Boolean is_selected = checkboxSelect.get(0).isSelected();
			{
			 if (is_selected == true) {
				 checkboxSelect.get(1).click();

		      } else {checkboxSelect.get(0).click();
		      }
			}
			
	}
	
	

	public class PopupStudentList extends QAFWebComponent{
		
		@FindBy(locator="xpath=//h4[text()='Student List']")
		public QAFWebElement headerStudentList;
	
		
		public PopupStudentList(QAFExtendedWebElement parent, String locator) {
			super(parent, locator);
				// TODO Auto-generated constructor stub
			}
			public PopupStudentList(String locator) {
				super(locator);
				// TODO Auto-generated constructor stub
			}
			public void waitForPopupToLoad() {
				try {
					headerStudentList.waitForPresent();
					headerStudentList.waitForVisible();
					
					Reporter.log(this.getClass().getSimpleName() + " Popup loaded Sucessfully");
				}catch(Exception e) {
					Reporter.log(this.getClass().getSimpleName() + " Popup Did not Load");
				}
		
			}
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}}


	


