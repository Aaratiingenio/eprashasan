package com.ingenio.eprashasan.full.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanPayrollLeavePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "xpath=//input[@id=\"leaveType\"]")
	public QAFWebElement textboxLeaveType;
	
	@FindBy(locator = "xpath=//input[@id=\"formulaName\"]")
	public QAFWebElement textboxFormulaName;
	
	@FindBy(locator = "xpath=//input[@id=\"addStandbtn\"]")
	public QAFWebElement buttonSave;
	
	@FindBy(locator = "xpath=//input[@id=\"deletebtn\"]")
	public QAFWebElement buttonDelete;
	
	@FindBy(locator = "xpath=//input[@id=\"reset\"]")
	public QAFWebElement buttonReset;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
