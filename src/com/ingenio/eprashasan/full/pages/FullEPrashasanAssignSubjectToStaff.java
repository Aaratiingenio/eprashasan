package com.ingenio.eprashasan.full.pages;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.ingenio.eprashasan.core.EPUtils;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.thoughtworks.selenium.webdriven.commands.Click;
import static org.testng.Assert.assertEquals;

public class FullEPrashasanAssignSubjectToStaff extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	
	
	
	private static final String isRecordInserted = null;

	@FindBy(locator = "xpath=//h4[text()='Assign Subject To Staff']")
	public QAFWebElement headerAssignSubjectToStaff;
	
	@FindBy(locator = "xpath=//select[@id='yearcomboID']")
	public DropdownListWebElement dropdownYear;
	
	@FindBy(locator = "xpath=.//*[@id='staffSubjectDetailsDivId']")
	public QAFWebElement tableStaffSubjectDetails;

	@FindBy(locator = "xpath=//input[@id='teacherName1']")
	public QAFWebElement textboxStaffName;
	
	@FindBy(locator = "xpath=//input[@id='checkbox1']")
	public QAFWebElement checkboxIsClassTeacher;
	
	@FindBy(locator = "xpath=//td[@class='myButtonGreen']")
	public QAFWebElement textfieldDisplayColor;
	
   
	
	
	
	
	public void selectOptionWithText() {
		boolean isRecordInserted=false;
    	
     	try {
		
			  WebElement select = driver.findElement(By.id("ui-id-2"));
			   
			   List<WebElement> options = select.findElements(By.xpath("//a[text()='Mr. Jayesh K Shikre']"));
			   if(CollectionUtils.isEmpty(options)) {
					Reporter.log("Failed to execute Compare Data");
					System.err.println("Failed to Execute");
			   }
			   for (WebElement option1 : options) {

			   if("Mr. Jayesh K Shikre".equals(option1.getText().trim())) {
			isRecordInserted=true;
			   option1.click();
			   EPUtils.sleep(3000);
			   
			   Reporter.log("User Clicked On 1st option");
			   break;
			   } 
			   assertEquals(true,isRecordInserted,"No Record Inserted");   
			   
    	
    	}
		}catch (Exception e){
		Reporter.log("Failed to execute Compare Data");
		System.err.println("Failed to Execute");
		}
    	
	}
	
	public void verifytextboxcolor() {

 
		String color = driver.findElement(By.id("column1")).getCssValue("color");
		String[] hexValue = color.replace("rgba(", "").replace(")", "").split(",");
		 
		int hexValue1=Integer.parseInt(hexValue[0]);
		hexValue[1] = hexValue[1].trim();
		int hexValue2=Integer.parseInt(hexValue[1]);
		hexValue[2] = hexValue[2].trim();
		int hexValue3=Integer.parseInt(hexValue[2]);
		 
		String actualColor = String.format("#245dc1", hexValue1, hexValue2, hexValue3);
		 
		Assert.assertEquals("#245dc1", actualColor);
		
		}
	
	
	
	
	
   @Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	@Override
	public void waitForPageToLoad() {
		Reporter.log("On Page "+this.getClass().getSimpleName());
		super.waitForAjaxToComplete();
		try {
			headerAssignSubjectToStaff.waitForVisible();
			headerAssignSubjectToStaff.waitForPresent();
			Reporter.log(this.getClass().getSimpleName() + " Page loaded Sucessfully");
			
		} catch (Exception e) {
			Assert.fail(this.getClass().getSimpleName() + " Page Did not Load");
		}
	  }
}
