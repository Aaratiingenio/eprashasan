package com.ingenio.eprashasan.full.pages;

import com.ingenio.eprashasan.core.DropdownListWebElement;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FullEPrashasanAccountGroups extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "xpath=//span[@id=\"hideAsPerCschool\"]/img")
	public QAFWebElement imgCreateNewGroup;

	@FindBy(locator = "xpath=//select[@id=\"head\"]")
	public DropdownListWebElement selectHead;
	
	@FindBy(locator = "xpath=//select[@id=\"subhead\"]")
	public DropdownListWebElement selectSubHead;
	
	@FindBy(locator = "xpath=//input[@id=\"groupSubgroup\"]")
	public QAFWebElement textboxAddNewGroup;
	
	@FindBy(locator = "xpath=(//div[@class=\"scrollit\"]//img[@src=\"/ePrashasanE1/resources/account/img/add.png\"])[1]")
	public QAFWebElement imgNewGroup;
	
	@FindBy(locator = "xpath=//input[@id=\"checkBoxDeleteIds1\"]")
	public QAFWebElement checkboxGroup;
	
	@FindBy(locator = "xpath=(//img[@src=\"/ePrashasanE1/resources/account/img/delete.png\"])[1]")
	public QAFWebElement imgDelete;
	
	@FindBy(locator = "xpath=((//div[@class=\"scrollit\"]//img[@src=\"/ePrashasanE1/resources/account/img/add.png\"])[2]")
	public QAFWebElement imgAdd;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Bank Accounts\")]")
	public QAFWebElement tabBankAccounts;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Bank OD\")]")
	public QAFWebElement tabBankOD;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Branch / Divisions\")]")
	public QAFWebElement tabBranchDivisions;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Capital Account\")]")
	public QAFWebElement tabCapitalAccount;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Cash-in-hand\")]")
	public QAFWebElement tabCashInHand;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Current Assets\")]")
	public QAFWebElement tabCurrentAssets;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Current Liabilities\")]")
	public QAFWebElement tabCurrentLiabilities;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Deposits (Asset)\")]")
	public QAFWebElement tabDepositsAsset;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Direct Incomes\")]")
	public QAFWebElement tabDirectIncomes;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Direct Expenses\")]")
	public QAFWebElement tabDirectExpenses;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Duties & Taxes\")]")
	public QAFWebElement tabDutiesTaxes;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Estabshment Expen\")]")
	public QAFWebElement tabEstabshmentExpen;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Expenses on Object OF trust\")]")
	public QAFWebElement tabExpensesTrust;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Fixed Assets\")]")
	public QAFWebElement tabFixedAssests;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Indirect Expenses\")]")
	public QAFWebElement tabIndirectExpenses;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Indirect Incomes\")]")
	public QAFWebElement tabIndirectIncomes;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Investments\")]")
	public QAFWebElement tabInvestments;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Loans & Advances (Asset)\")]")
	public QAFWebElement tabLoanAdvancesAsset;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Loans (Liability)\")]")
	public QAFWebElement tabLoansLiability;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Misc. Expenses (ASSET)\")]")
	public QAFWebElement tabMiscExpensesAsset;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Provisions\")]")
	public QAFWebElement tabProvisions;
	
	@FindBy(locator = "xpath=//a[contains(.,\"P & L / Surplus or Deficit\")]")
	public QAFWebElement tabSurplusDeficit;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Purchase Accounts\")]")
	public QAFWebElement tabPurchaseAccounts;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Reserves & Surplus\")]")
	public QAFWebElement tabReservesSurplus;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Sales Accounts\")] ")
	public QAFWebElement tabSalesAccounts;
	
	@FindBy(locator = "xpath=//a[text()=\"Secured Loans\"] ")
	public QAFWebElement tabSecuredLoans;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Stock-in-hand\")]")
	public QAFWebElement tabStockInHand;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Sundry Creditors\")]")
	public QAFWebElement tabSundryCreditors;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Sundry Debtors\")]")
	public QAFWebElement tabSundryDebators;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Suspence AC\")]")
	public QAFWebElement tabSuspenceAC;
	
	@FindBy(locator = "xpath=//a[contains(.,\"UnSecured Loans\")]")
	public QAFWebElement tabUnSecuredLoans;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Contingeceies\")]")
	public QAFWebElement tabContingeceies;
	
	@FindBy(locator = "xpath=//a[contains(.,\"Other / Misc.  Incom\")]")
	public QAFWebElement tabOtherMiscIncom;
	
	@FindBy(locator = "xpath=//a[text()=\"Misc. Expenses\"]")
	public QAFWebElement tabMiscExpenses;
	
	@FindBy(locator = "xpath=(//img[@src=\"/ePrashasanE1/resources/account/img/print.png\"])[1]")
	public QAFWebElement buttonPrint;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
