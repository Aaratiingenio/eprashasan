package com.ingenio.eprashasan.english.full.testsuits.development;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.testng.annotations.Test;

import com.ingenio.eprashasan.core.EPUtils;
import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;
import com.ingenio.eprashasan.full.pages.FullSansthaRegistrationHomePage;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class SansthaRegistration extends WebDriverTestCase {

	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registration Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality for blank data\r\n" +"")
	public void TC_Sanstha_01(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		fullSansthaRegistrationHomePage.buttonSubmit.click();
	
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Sanstha Name"), 
				"Please Enter Sanstha Name Alert message not displayed",
				"Please Enter Sanstha Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
			}
	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Sanstha Reg No\"")
	public void TC_Sanstha_02(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Reg No"), 
				"Please Enter Reg No Alert message not displayed",
				"Please Enter Reg No Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Sanstha Key\"")
	public void TC_Sanstha_03(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Sanstha Key"), 
				"Please Enter Sanstha Key Alert message not displayed",
				"Please Enter Sanstha Key Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}
    @Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Mobile No.\"")
	public void TC_Sanstha_04(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Contact No."), 
				"Please Enter Contact No. Alert message not displayed",
				"Please Enter Contact No. Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	
    @Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"First Name.\"")
	public void TC_Sanstha_05(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));		
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter First Name"), 
				"Please Enter First Name Alert message not displayed",
				"Please Enter First Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	

	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Last Name.\"")
	public void TC_Sanstha_06(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Last Name"), 
				"Please Enter Last Name Alert message not displayed",
				"Please Enter Last Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	

	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Username\"")
	public void TC_Sanstha_07(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
		fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter User Name"), 
				"Please Enter User Name Alert message not displayed",
				"Please Enter User Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	
    @Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Password\"")
	public void TC_Sanstha_08(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Password"), 
				"Please Enter Password Alert message not displayed",
				"Please Enter Password Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	
    @Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Question1\"")
	public void TC_Sanstha_09(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
    	fullSansthaRegistrationHomePage.textboxpasswordId.sendKeys(data.get("password"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Select Question 1"), 
				"Please Select Question 1 Alert message not displayed",
				"Please Select Question 1 Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Answer1\"")
	public void TC_Sanstha_10(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
    	fullSansthaRegistrationHomePage.textboxpasswordId.sendKeys(data.get("password"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion1.selectByIndex(3);
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Answer 1"), 
				"Please Enter Answer 1 Alert message not displayed",
				"Please Enter Answer 1 Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	

	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Question2\"")
	public void TC_Sanstha_11(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
    	fullSansthaRegistrationHomePage.textboxpasswordId.sendKeys(data.get("password"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion1.selectByIndex(3);
		fullSansthaRegistrationHomePage.textboxanswer1.sendKeys(data.get("answer1"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Select Question 2"), 
				"Please Select Question 2 Alert message not displayed",
				"Please Select Question 2 Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	

	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields except \"Answer2\"")
	public void TC_Sanstha_12(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
    	fullSansthaRegistrationHomePage.textboxpasswordId.sendKeys(data.get("password"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion1.selectByIndex(3);
		fullSansthaRegistrationHomePage.textboxanswer1.sendKeys(data.get("answer1"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion2.selectByIndex(2);
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter Answer 2"), 
				"Please Enter Answer 2 Alert message not displayed",
				"Please Enter Answer 2 Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
}	

	// Sanstha Registration for actual data
	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields ")
	public void TC_Sanstha_13(Map<String, String> data) throws InterruptedException, ClassNotFoundException, FileNotFoundException, SQLException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("contactno"));
		fullSansthaRegistrationHomePage.textboxState.sendKeys(data.get("state"));
		fullSansthaRegistrationHomePage.textboxPincode.sendKeys(data.get("pincode"));
		fullSansthaRegistrationHomePage.textboxEmail.sendKeys(data.get("emailid"));
		fullSansthaRegistrationHomePage.textboxFax.sendKeys(data.get("fax"));
		fullSansthaRegistrationHomePage.textboxAddress.sendKeys(data.get("address"));
		fullSansthaRegistrationHomePage.textboxCity.sendKeys(data.get("city"));
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
    	fullSansthaRegistrationHomePage.textboxpasswordId.sendKeys(data.get("password"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion1.selectByIndex(3);
		fullSansthaRegistrationHomePage.textboxanswer1.sendKeys(data.get("answer1"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion2.selectByIndex(2);
		fullSansthaRegistrationHomePage.textboxanswer2.sendKeys(data.get("answer2"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();
		
		
		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		EPUtils.sleep(3000);
		verifyTrue(Windowalert.getText().contains("Sanstha Added Successfully"), 
				"Sanstha Added Successfully message not displayed",
				"Sanstha Added Successfully message displayed");
		
		getDriver().switchTo().alert().accept();
		GetSQLData.getSansthaRegistartion(data);
		
		//RunSqlScript.executeQueryBySqlFile();
		}	

	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate Sanstha Regi.No Text Field")
	public void TC_Sanstha_14(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("duplicate_sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthakey"));
		
			
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Registration No Already Exists...!"), 
				"Registration No Already Exists...! Alert message not displayed",
				"Registration No Already Exists...! Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}	

	
	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate Sanstha Key Text Field")
	public void TC_Sanstha_15(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
//		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sanstharegno"));
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("duplicate_sansthakey"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys(data.get("mobileno"));
		
			
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Sanstha Key Already Exists...!"), 
				"Sanstha Key Already Exists...! Alert message not displayed",
				"Sanstha Key Already Exists...! Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}	
	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by entering all mandetory fields with invalid (less than 10 digit) \"Mobile No.\" ")
	public void TC_Sanstha_16(Map<String, String> data) throws InterruptedException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User started filling details");
		
		fullSansthaRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaname"));
		fullSansthaRegistrationHomePage.textboxSansthaRegNo.sendKeys("0040");
		fullSansthaRegistrationHomePage.textboxSansthaKey.sendKeys("0040");
		fullSansthaRegistrationHomePage.textboxWebsite.sendKeys(data.get("website"));
		fullSansthaRegistrationHomePage.textboxContactNo.sendKeys("741245785");
		
		fullSansthaRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstname"));
    	fullSansthaRegistrationHomePage.textboxLastName.sendKeys(data.get("lastname"));
		fullSansthaRegistrationHomePage.textboxmyUserNameId.sendKeys(data.get("username"));
    	fullSansthaRegistrationHomePage.textboxpasswordId.sendKeys(data.get("password"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion1.selectByIndex(3);
		fullSansthaRegistrationHomePage.textboxanswer1.sendKeys(data.get("answer1"));
		fullSansthaRegistrationHomePage.dropdownboxQuestion2.selectByIndex(2);
		fullSansthaRegistrationHomePage.textboxanswer2.sendKeys(data.get("answer2"));
		fullSansthaRegistrationHomePage.buttonSubmit.click();

		Reporter.log("User Clicked on button Submit");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Mobile No. is not valid, Please Enter 10 Digit Mobile No."), 
				"Mobile No. is not valid, Please Enter 10 Digit Mobile No. Alert message not displayed",
				"Mobile No. is not valid, Please Enter 10 Digit Mobile No. Alert message displayed");
		
		getDriver().switchTo().alert().accept();
			
	}	
	
	
	
	@Test
	@QAFDataProvider(key="sanstharegistration.data")
	@MetaData(value = "{'groups':['Sanstha Registartion Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Already Registered\"link by clicking on it ")
	public void TC_Sanstha_17(Map<String, String> data) throws InterruptedException, ClassNotFoundException, FileNotFoundException, SQLException {
	
		FullSansthaRegistrationHomePage fullSansthaRegistrationHomePage = new FullSansthaRegistrationHomePage();
		fullSansthaRegistrationHomePage.invoke("/error/openTrustRegistrationPage.do");
		EPUtils.sleep(2000);
		fullSansthaRegistrationHomePage.waitForPageToLoad();
		Reporter.log("User clicked on registration link");
		fullSansthaRegistrationHomePage.linkAlreadyRegistrerd.click();
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.textboxUsername.sendKeys(data.get("username"));
		Reporter.log("User Entered Username");
		fullEPrashasanLoginPage.textboxPassword.sendKeys(data.get("password"));
		Reporter.log("User Entered valid Password");
		fullEPrashasanLoginPage.buttonLogin.click();
		Reporter.log("User Clicked on Login Button");
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		verifyTrue(fullEPrashasanWelcomePage.headerSansthaName.isPresent(),
	 
		"User not navigated to Welcome Page", "User navigated to Welcome Page");
		
		GetSQLData.getSansthaRegistartion(data);
		
//	   RunSqlScript.executeQueryBySqlFile();
		
	}

}
