package com.ingenio.eprashasan.english.full.testsuits.development;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.ingenio.eprashasan.core.EPUtils;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAddBatchPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanExamMasterSettingPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class Exam extends WebDriverTestCase {
	
	 WebDriver driver;
   
	@Test
	@QAFDataProvider(key="login.data")
    @QAFTestStep(description ="Verify Add Batch Button functionality to add batch for selected criteria")
	public  void Exam_AdminSett_SubSett_AddBatch_01(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(1);
		Reporter.log("User clicked on add batch button");
		fullEPrashasanAddBatchPage.buttonAddBatch.click();
		
		fullEPrashasanAddBatchPage.popupAddBatch.waitForVisible();
			
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save Batch button Functionality without entering batch name")
	public  void Exam_AdminSett_SubSett_AddBatch_02(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		Reporter.log("User clicked on add batch button");
		fullEPrashasanAddBatchPage.buttonAddBatch.click();
		
		fullEPrashasanAddBatchPage.popupAddBatch.waitForVisible();
		
		fullEPrashasanAddBatchPage.popupAddBatch.buttonSaveBatch.click();	

        Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Please enter batch Name"),
				"Please enter batch Name Alert message not displayed",
				"Please enter batch Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
		}
	 
	@Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save Batch button Functionality  entering batch name only ")
	public  void Exam_AdminSett_SubSett_AddBatch_03(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		Reporter.log("User clicked on add batch button");
		fullEPrashasanAddBatchPage.buttonAddBatch.click();
		
		fullEPrashasanAddBatchPage.popupAddBatch.waitForVisible();
		fullEPrashasanAddBatchPage.popupAddBatch.textboxBatchName.sendKeys("Batch 1");
	    fullEPrashasanAddBatchPage.popupAddBatch.buttonSaveBatch.click();	
    	Reporter.log("User clicked on save batch button");
    	EPUtils.sleep(1000);
        fullEPrashasanAddBatchPage.tabBatch.waitForVisible();
        Reporter.log("User verified batch tab");
                
	}
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save  button Functionality updating batch name ")
	public  void Exam_AdminSett_SubSett_AddBatch_04(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		fullEPrashasanAddBatchPage.tabBatch.waitForVisible();
		fullEPrashasanAddBatchPage.textboxBatchName.clear();
		fullEPrashasanAddBatchPage.textboxBatchName.sendKeys("Marathi Batch 1");
		Reporter.log("User updated batch name");
		
	    fullEPrashasanAddBatchPage.buttonSave.click();	
    	Reporter.log("User clicked on save batch button");
    	EPUtils.sleep(1000);
    	
    	Alert windowalert=getDriver().switchTo().alert();
    	verifyTrue(windowalert.getText().contains("Data Saved Successfully"),
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		            
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save  button Functionality updating batch name ")
	public  void Exam_AdminSett_SubSett_AddBatch_05(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		fullEPrashasanAddBatchPage.tabBatch.waitForVisible();
		fullEPrashasanAddBatchPage.textboxBatchName.clear();
	
	    fullEPrashasanAddBatchPage.buttonSave.click();	
    	Reporter.log("User clicked on save batch button");
    	EPUtils.sleep(1000);
    	
    	Alert windowalert=getDriver().switchTo().alert();
    	verifyTrue(windowalert.getText().contains("Please enter batch Name"),
				"Please enter batch Name Alert message not displayed",
				"Please enter batch Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		        
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save  button Functionality updating batch name ")
	public  void Exam_AdminSett_SubSett_AddBatch_06(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		fullEPrashasanAddBatchPage.tabBatch.waitForVisible();
		fullEPrashasanAddBatchPage.buttonDelete.click();
	 
    	Reporter.log("User clicked on delete batch button");
    	EPUtils.sleep(1000);
    	
    	Alert windowalert=getDriver().switchTo().alert();
    	verifyTrue(windowalert.getText().contains("Record Deleted Successfully"),
				"Record Deleted Successfully Alert message not displayed",
				"Record Deleted Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Assign Student Link functionaliy by clicking on it for selected batch ")
	public  void Exam_AdminSett_SubSett_AddBatch_07(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		fullEPrashasanAddBatchPage.buttonAddBatch.click();
		fullEPrashasanAddBatchPage.popupAddBatch.waitForPresent();
		fullEPrashasanAddBatchPage.popupAddBatch.textboxBatchName.sendKeys("Batch 5");
		fullEPrashasanAddBatchPage.popupAddBatch.buttonSaveBatch.click();
//		fullEPrashasanAddBatchPage.tabBatch.waitForVisible();
		fullEPrashasanAddBatchPage.linkAssignStudent.click();
		Reporter.log("User Clicked on Assign Student");
		
		Reporter.log("User verified StudentList Popup");	 
		EPUtils.sleep(2000);
	}
	
    @Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save Batch button Functionality  entering batch name and select all functionality  ")
	public  void Exam_AdminSett_SubSett_AddBatch_08(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(1);
		Reporter.log("User clicked on add batch button");
		fullEPrashasanAddBatchPage.buttonAddBatch.click();
		
		fullEPrashasanAddBatchPage.popupAddBatch.waitForVisible();
		fullEPrashasanAddBatchPage.popupAddBatch.textboxBatchName.sendKeys("Batch 10");
	
		fullEPrashasanAddBatchPage.popupAddBatch.checkBoxSelectAll.click();
		Reporter.log("User clicked on select all chekbox");
	    fullEPrashasanAddBatchPage.popupAddBatch.buttonSaveBatch.click();	
    	Reporter.log("User clicked on save batch button");
    	EPUtils.sleep(1000);
        fullEPrashasanAddBatchPage.tabBatch.waitForVisible();
               
	}
    @Test
	@QAFDataProvider(key="login.data")
	@QAFTestStep(description ="Save Batch button Functionality  entering batch name and select all functionality  ")
	public  void Exam_AdminSett_SubSett_AddBatch_09(Map<String, String> data){
		
		FullEPrashasanLoginPage FullEPrashasanLoginPage= new FullEPrashasanLoginPage();
		FullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage=new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkExam.click();
		Reporter.log("User Clicked on Exam module link ");
		EPUtils.sleep(2000);
		
		FullEPrashasanExamMasterSettingPage fullEPrashasanExamMasterSettingPage=new FullEPrashasanExamMasterSettingPage();
//		fullEPrashasanExamMasterSettingPage.headerExamMasterSetting.waitForPresent();
		
		fullEPrashasanExamMasterSettingPage.linkAdminSetting.click();
		Reporter.log("User Clicked on Admin setting link ");
		fullEPrashasanExamMasterSettingPage.linkSubjectSetting.click();
		Reporter.log("User Clicked on subject setting link ");
		EPUtils.sleep(2000);
		fullEPrashasanExamMasterSettingPage.linkAddBatch.click();
		Reporter.log("User Clicked on Add Batch link ");
		
        FullEPrashasanAddBatchPage fullEPrashasanAddBatchPage=new FullEPrashasanAddBatchPage();	
		fullEPrashasanAddBatchPage.headerAddBatch.waitForVisible();
		Reporter.log("User started filling details");
		fullEPrashasanAddBatchPage.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanAddBatchPage.dropdownSelectCourse.selectByIndex(2);
		fullEPrashasanAddBatchPage.buttonAddBatch.click();
		Reporter.log("User clicked on add batch button");
   	   fullEPrashasanAddBatchPage.popupAddBatch.waitForPopupToLoad();
   	 /*fullEPrashasanAddBatchPage.popupAddBatch.headerAddBatch.waitForPresent();
		Reporter.log("User verifies Add Batch Pop up is loaded ");
		fullEPrashasanAddBatchPage.popupAddBatch.textboxBatchName.sendKeys("Batch 1");
		Reporter.log("User entered batch name");
		
		fullEPrashasanAddBatchPage.popupAddBatch.isSelected();
		Reporter.log("User selected student checkbox");
		EPUtils.sleep(2000);
		

		Reporter.log("User clicked on select all chekbox");
	    fullEPrashasanAddBatchPage.popupAddBatch.buttonSaveBatch.click();	
    	Reporter.log("User clicked on save batch button");
    	EPUtils.sleep(1000);
        fullEPrashasanAddBatchPage.tabBatch.waitForVisible();*/
              
	}
    
    
		
}