package com.ingenio.eprashasan.english.full.testsuits.development;

import java.util.Map;

import org.openqa.selenium.Alert;
import org.testng.annotations.Test;

import com.ingenio.eprashasan.core.EPUtils;
import com.ingenio.eprashasan.full.pages.FullEPrashasanEditStudentInformationPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanQuickRegistrationFormPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStudentAdmissionFormPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class ADMISSION extends WebDriverTestCase{

	@Test
	@QAFDataProvider(key="quick.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by entering all mandatory fields  with Reg No as Provisional Option.")
	public void TC_Admis_Quick_01(Map<String, String> data) { 
		
		
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		//fullEPrashasanLoginPage.waitForPageToLoad();;
		
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByOptionValue("Maratha");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OPEN");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		
		
		
		
		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on button Add");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
				"Are You Sure? Alert message not displayed",
				"Are You Sure? Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		verifyTrue(Windowalert.getText().contains("Record Inserted Successfully"), 
				"Record Inserted Successfully message not displayed",
				"Record Inserted Successfully message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		
		GetSQLData.getSchoolMaster(data);
		GetSQLData.getStudentMaster(data);
		
		
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="quick.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by entering all mandatory fields with Reg No as Confirm Option.")
	public void TC_Admis_Quick_02(Map<String, String> data) { 
		
		
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		//fullEPrashasanLoginPage.waitForPageToLoad();;
		
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible(2000);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByOptionValue("Maratha");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OPEN");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		
		
		
		
		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on button Add");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
				"Are You Sure? Alert message not displayed",
				"Are You Sure? Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		verifyTrue(Windowalert.getText().contains("Record Inserted Successfully"), 
				"Record Inserted Successfully message not displayed",
				"Record Inserted Successfully message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		
		GetSQLData.getSchoolMaster(data);
		GetSQLData.getStudentMaster(data);
		
		
	}
	
	
	
	
	
	
	
	@Test
	@QAFDataProvider(key="quick.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Cancel\" buttun functionality by entering all mandatory fields")
	public void TC_Admis_Quick_03(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByOptionValue("2019-2020");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByOptionValue("Maratha");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
		
		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on button Add");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
				"Are You Sure? Alert message not displayed",
				"Are You Sure? Alert message displayed");
		
		getDriver().switchTo().alert().dismiss();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked On Cancel button");
		
	}
	
	@Test
	@QAFDataProvider(key="quick.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by without selecting Academic year")
	public void TC_Admis_Quick_04(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		//Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
	}
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality with \"Registration Number\" mandatory field blank")
	public void TC_Admis_Quick_05(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		//Reporter.log("User started filling details");
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
				
				//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
				fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
				
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality \"First Name\" field blank")
	public void TC_Admis_Quick_06(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality \"Middle Name\" field blank")
	public void TC_Admis_Quick_07(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality \"Middle Name\" field blank")
	public void TC_Admis_Quick_08(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by without selecting  \"Date\" in \"Admission Date\" drop down list")
	public void TC_Admis_Quick_09(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by without selecting  \"Month\" in \"Admission Date\" drop down list")
	public void TC_Admis_Quick_10(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by without selecting  Year value in \"Admission Date\" drop down list")
	public void TC_Admis_Quick_11(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
	
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by selecting invalid date for a month")
	public void TC_Admis_Quick_12(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(31);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys("7385120120");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on Add Button");
		EPUtils.sleep(3000);
		
	
         Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Select Valid Admission Day"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
		
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by without selecting  Class value in \"Class\" drop down list")
	public void TC_Admis_Quick_13(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(5);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys("7385120120");
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on Add Button");
		EPUtils.sleep(3000);
		
	
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");
		
		
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="login.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by selecting invalid date for a month(Birth Date)")
	public void TC_Admis_Quick_14(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(31);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(4);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on Add Button");
		EPUtils.sleep(3000);
		
	
		
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Select Valid Birth Day"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality by selecting invalid date for a month(Birth Date)")
	public void TC_Admis_Quick_15(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("duplicateRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(19);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(4);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on Add Button");
		EPUtils.sleep(3000);
		
	
		
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("This Registration Number Is Already Allocated"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" button functionality by entering all type(Alphanumeric,Special Charecters) data in \"Registration Number\" field")
	public void TC_Admis_Quick_16(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("alphanumericRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(19);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(4);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
		
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		Reporter.log("User Clicked on Add Button");
		EPUtils.sleep(3000);
		
		
		
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
		
		
Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Inserted Successfully"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(2000);
		
		
		
		//GetSQLData getSQLData = new GetSQLData();
		//GetSQLData.getSchoolMaster(data);
		//GetSQLData.getStudentMaster(data);
		
		
		
		
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Verify Inserted data in the Admission Form through Database")
	public void TC_Admis_Quick_16_1(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
	
	}	
	
	
	
	//************************* Assign And Pay Fees Test Cases are Remaining **************************
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate Reset functionality")
	public void TC_Admis_Quick_64(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		Reporter.log("User started filling details");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.waitForVisible();
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("alphanumericRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxStudentFormNumber.sendKeys(data.get("formNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownInitialName.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.sendKeys(data.get("middleName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.sendKeys(data.get("lastName"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(15);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(6);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthDay.selectByIndex(19);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthMonth.selectByIndex(4);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownBirthYear.selectByIndex(12);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownGender.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCaste.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownCategory.selectByOptionValue("OBC");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownReligion.selectByOptionValue("Hindu");
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.sendKeys(data.get("mobileNo"));
		
		
		
					
		fullEPrashasanQuickRegistrationFormPage.buttonReset.click();
		Reporter.log("User Clicked on Reset Button");
		EPUtils.sleep(3000);
		
		
		
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
		
		
//Alert Windowalert1 = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert1.getText().contains("Record Inserted Successfully"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
		
		
	}
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"For Edit Click Here\" link functionality link functionality")
	public void TC_Admis_Quick_65(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		
	}	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality without selecting any criteria")
	public void TC_Admis_Quick_66(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(4000);
		
	}	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting \"Academic Year\" for which record exist")
	public void TC_Admis_Quick_67(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(4000);
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting \"Academic Year\" for which record is not exist")
	public void TC_Admis_Quick_68(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(1);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		//Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting \"Class\" for which record exist")
	public void TC_Admis_Quick_69(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(4000);
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting \"Class\" for which record is not exist")
	public void TC_Admis_Quick_70(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(3);
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		//Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting \"Category\" for which record exist")
	public void TC_Admis_Quick_71(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(8);;
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		//Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting \"Category\" for which record is not exist")
	public void TC_Admis_Quick_72(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		//Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate Autocomplete functionality by entering name in the \"Student Name\" field")
	public void TC_Admis_Quick_73(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxStudentName.sendKeys("des");
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
//		//Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by selecting name from autocomplete list")
	public void TC_Admis_Quick_74(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxStudentName.sendKeys("des");
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by without selecting name from autocomplete list")
	public void TC_Admis_Quick_75(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxStudentName.sendKeys("des");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by entering present student(Not left) registration number in \"Registration Number\" field")
	public void TC_Admis_Quick_76(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys(data.get("regNo"));
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" functionality by entering (Left Student) registration number in \"Registration Number\" field")
	public void TC_Admis_Quick_77(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P1");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" button functionality by entering registration number which include all type data for which record exist")
	public void TC_Admis_Quick_78(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" button functionality by selecting gender as \"Male\" from \"Gender\" drop down list")
	public void TC_Admis_Quick_79(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(1);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Search\" button functionality by selecting gender as \"Female\" from \"Gender\" drop down list")
	public void TC_Admis_Quick_80(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Edit\" functionality of quick registration form by without selecting record")
	public void TC_Admis_Quick_81(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Select Student"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Edit\" functionality of quick registration form by  selecting record")
	public void TC_Admis_Quick_82(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate the pagination functionality")
	public void TC_Admis_Quick_83(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Information Not Found..!"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	/*******************Test Cases 84 & 85 are only to verify displayed data********************/
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" functionality of the editing form of the \"Quick Registration\" form by entering data in all form")
	public void TC_Admis_Quick_86_1(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().dismiss();
		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" functionality of the editing form of the \"Quick Registration\" form by entering data in all form")
	public void TC_Admis_Quick_86_2(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" functionality of the editing form of the \"Quick Registration\" form by entering data in all form")
	public void TC_Admis_Quick_87(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(2000);
		
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();		
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate editability of the \"Academic Year\" of the editing form of the \"Quick Registration\" form ")
	public void TC_Admis_Quick_88(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
//		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
//		Alert Windowalert = getDriver().switchTo().alert();
//		
//		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
//						"Alert message not displayed",
//						"Alert message displayed");
//		getDriver().switchTo().alert().accept();		
	}
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" button functionality of the editing form of the \"Quick Registration\" form  with \"Registration Number\" mandatory field blank")
	public void TC_Admis_Quick_89(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form  with \"First Name\" field blank")
	public void TC_Admis_Quick_90(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.clear();
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form  with \"Middle Name\" field blank")
	public void TC_Admis_Quick_91(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.clear();
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.clear();
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form  with \"Last Name\" field blank")
	public void TC_Admis_Quick_92(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.clear();
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMiddleName.clear();
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName.clear();
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxLastName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form   by without selecting  \"Date\" in \"Admission Date\" drop down list")
	public void TC_Admis_Quick_93(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(0);
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form   by without selecting  \"Month\" in \"Admission Date\" drop down list")
	public void TC_Admis_Quick_94(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth.selectByIndex(0);
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionMonth);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form   by without selecting  \"Year\" in \"Admission Date\" drop down list")
	public void TC_Admis_Quick_95(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear.selectByIndex(0);
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionYear);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form by selecting invalid date for a month")
	public void TC_Admis_Quick_96(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		//fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAdmissionDay.selectByIndex(31);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Select Valid Admission Day"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();	
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality  of the editing form of the \"Quick Registration\" form by without selecting  Class value in \"Class\" drop down list")
	public void TC_Admis_Quick_97(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("newRegNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please select an item in the list."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form by entering duplicate Registration Number")
	public void TC_Admis_Quick_107(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("This Registration Number Is Already Allocated"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form with \"Father Name\" field blank")
	public void TC_Admis_Quick_108(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form with \"Mother Name\" field blank")
	public void TC_Admis_Quick_109(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
//		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form with \"Mothertongue\" field blank")
	public void TC_Admis_Quick_110(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.basicDetailsComponent.textboxMothertonge.clear();
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.basicDetailsComponent.textboxMothertonge);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form with \"Nationality\" field blank")
	public void TC_Admis_Quick_111(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		fullEPrashasanQuickRegistrationFormPage.basicDetailsComponent.textboxNationlity.clear();
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.basicDetailsComponent.textboxNationlity);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form with \"Contact Number 1 For SMS\" field blank")
	public void TC_Admis_Quick_112(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo.clear();
		EPUtils.sleep(2000);
		
		
Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter the Contact Number"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
		
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxMobileNo);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Add\" buttun functionality of the editing form of the \"Quick Registration\" form with \"Previous School Name\" field blank")
	public void TC_Admis_Quick_113(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
				
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		
//		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonAdd.click();
		EPUtils.sleep(2000);
		
		
		
		
		String errorMessage = EPUtils.getFieldLevelErrorValidationMessage(fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName);
		Reporter.log(errorMessage);
		
		verifyTrue(errorMessage.equals("Please fill out this field."), 
				"Fail : Message "+errorMessage+" not displayed",
				"Pass : Message "+errorMessage+" displayed");	
	}
	
	
	
	
	
	@Test 
	@QAFDataProvider(key="quick.data") 
	@MetaData(value = "{'groups':['Admission']}")
	@QAFTestStep(description = "Validate \"Reset\" functionality of the editing form of the \"Quick Registration\" form by modifying data")
	public void TC_Admis_Quick_114(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.waitForPageToLoad();
		//fullEPrashasanWelcomePage.headerSchoolName.verifyText("Welcome To e-PRASHASAN");
		Reporter.log("User Navigated to Welcome page");
		
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAdmission.click();
		Reporter.log("User Clicked on Addmission Link");
		
		FullEPrashasanStudentAdmissionFormPage fullEPrashasanStudentAdmissionFormPage = new FullEPrashasanStudentAdmissionFormPage();
		//fullEPrashasanStudentAdmissionFormPage.waitForPageToLoad();
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.dropdownlinkAdmission.click();
		Reporter.log("User Clicked on Admission Link");
		
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.waitForVisible();
		fullEPrashasanStudentAdmissionFormPage.topNavigationAdmissionPageComponent.linkQuickRegistration.click();
		Reporter.log("User Clicked on Quick Registration Link");
		
		
		
		FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
		
		
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.waitForVisible();
		fullEPrashasanQuickRegistrationFormPage.linkForEditClickHere.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on For Edit Click Here Link");
		EPUtils.sleep(2000);
		
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectYear.selectByIndex(2);
		EPUtils.sleep(4000);
		
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownGender.selectByIndex(2);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.textboxRegistrationNo.sendKeys("P2");
//		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.OptionSelectName1.click();
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownSelectClass.selectByIndex(1);
//		EPUtils.sleep(2000);
//		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownCategory.selectByIndex(7);;
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonSearch.click();
		Reporter.log("User Clicked on Search Button");
		EPUtils.sleep(2000);
//		//fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.dropdownPagination.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.optionSelect1.click();
		fullEPrashasanQuickRegistrationFormPage.searchStudentComponent.buttonEdit.click();
		EPUtils.sleep(2000);
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownAcademicYear.selectByIndex(3);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownRegistrationType.selectByIndex(1);
		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxRegistrationNumber.sendKeys(data.get("regNo"));
				
		
//		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.dropdownClass.selectByIndex(0);
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxFatherName.sendKeys("Amar");
		fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxMotherName.sendKeys("Ashwini");
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxEducation.sendKeys(data.get("education"));
//		//fullEPrashasanQuickRegistrationFormPage.parentDetailsComponent.textboxOccupation.sendKeys(data.get("occupation"));
		
		fullEPrashasanQuickRegistrationFormPage.previousSchoolDetailsComponent.textboxPreviousSchoolName.sendKeys(data.get("previousSchoolName"));
//		
		fullEPrashasanQuickRegistrationFormPage.buttonReset.click();
		EPUtils.sleep(2000);
		
		
		
		
Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();	
	}	
	
	
}
