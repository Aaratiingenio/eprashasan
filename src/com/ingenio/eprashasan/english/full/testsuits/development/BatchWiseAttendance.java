package com.ingenio.eprashasan.english.full.testsuits.development;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.ingenio.eprashasan.core.EPUtils;
import com.ingenio.eprashasan.full.pages.FullEPrashasanBatchAttendancePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanBatchAttendancePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanMarkAttendancePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffBasicDetailsFormPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffSchoolTypePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class BatchWiseAttendance extends WebDriverTestCase {

	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Verify Batch name by selection of the mandatory data")
    public void TC_Class_BatchWiseAtten_01(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.tabBatch.waitForVisible();
		Reporter.log("User verified batch tab ");
		EPUtils.sleep(1000);
	}
		
	// Add session Pop Up 
	
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Verify Add session Window by clicking on add session button for selected batch ")
    public void TC_Class_BatchWiseAttene_02(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		
	}
		
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button functionality without entering time on add session popup")
    public void TC_Class_BatchWiseAttene_03(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		
		fullEPrashasanBatchAttendancePage.popupAddsession.buttonSubmit.click();
		

		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Please enter In Time"),
				"Please enter In Time Alert message not displayed",
				"Please enter In Time Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
	}  
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button functionality by entering only in time")
    public void TC_Class_BatchWiseAttene_04(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxInTime.sendKeys("700AM");
		
		fullEPrashasanBatchAttendancePage.popupAddsession.buttonSubmit.click();
		
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Please enter Out Time"),
				"Please enter Out Time Alert message not displayed",
				"Please enter Out Time Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
	}
		
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button functionality entering In and Out time ")
    public void TC_Class_BatchWiseAttene_05(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		Reporter.log("User Clicked on Add Session button");
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxSessionName.clear();
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxSessionName.sendKeys("Session 1");
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxInTime.sendKeys("700AM");
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxOutTime.sendKeys("800AM");
		
		fullEPrashasanBatchAttendancePage.popupAddsession.buttonSubmit.click();
		
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Data saved Successfully"),
				"Data saved Successfully Alert message not displayed",
				"Data saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Time functionality entering Out  time is greater than in time ")
    public void TC_Class_BatchWiseAttene_06(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxInTime.sendKeys("700PM");
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxOutTime.sendKeys("800AM");
		
		fullEPrashasanBatchAttendancePage.popupAddsession.buttonSubmit.click();
		
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Out Time should be greater than In Time"),
				"Out Time should be greater than In Time Alert message not displayed",
				"Out Time should be greater than In Time Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
	}
	 
	   
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button functionality by not entering  Session Name")
    public void TC_Class_BatchWiseAttene_07(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxSessionName.clear();

		fullEPrashasanBatchAttendancePage.popupAddsession.textboxInTime.sendKeys("700PM");
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxOutTime.sendKeys("800PM");
		
		fullEPrashasanBatchAttendancePage.popupAddsession.buttonSubmit.click();
		
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Please Enter Session Name"),
				"Please Enter Session Name Alert message not displayed",
				"Please Enter Session Name Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button functionality by editing Session Name")
    public void TC_Class_BatchWiseAttene_08(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxSessionName.clear();
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxSessionName.sendKeys("Session 10");
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxInTime.sendKeys("700PM");
		fullEPrashasanBatchAttendancePage.popupAddsession.textboxOutTime.sendKeys("800PM");
		
		fullEPrashasanBatchAttendancePage.popupAddsession.buttonSubmit.click();
		
        Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Data Saved Successfully"),
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Close button Functionality on add session pop up")
    public void TC_Class_BatchWiseAttene_09(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
		fullEPrashasanBatchAttendancePage.buttonAddSession.click();
		fullEPrashasanBatchAttendancePage.popupAddsession.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupAddsession.headerAddSession.waitForVisible(); 
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonClose.click();
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Verify Mark Batch Wise  Attendance Pop Up generated by clicking on mark attendance link ")
    public void TC_Class_BatchWiseAttene_10(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
    	FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
	
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
//		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		fullEPrashasanBatchAttendancePage.linkMarkAttendance.click();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.waitForPopupToLoad();
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button Functionality without mark attendance")
    public void TC_Class_BatchWiseAttene_11(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		
		fullEPrashasanBatchAttendancePage.linkMarkAttendance.click();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.waitForPopupToLoad();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonSubmit.click();
	
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Data Saved Successfully"),
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
	}
		
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button Functionality to mark attendance by  selecting present button")
    public void TC_Class_BatchWiseAttene_12(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
	 
		fullEPrashasanBatchAttendancePage.linkMarkAttendance.click();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.waitForPopupToLoad();
//		
//		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonAbsent.click();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonPresent.click();
		
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonSubmit.click();
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Data Saved Successfully"),
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonClose.click();
		}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button Functionality to mark attendance by  selecting absent button")
    public void TC_Class_BatchWiseAtten_13(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		EPUtils.sleep(1000);
	 
		fullEPrashasanBatchAttendancePage.linkMarkAttendance.click();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.waitForPopupToLoad();
	
    	fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonAbsent.click();
		
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonSubmit.click();
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Data Saved Successfully"),
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonClose.click();
		
	}
	
	@Test
	@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Class-> Mark Attendance']}")
	@QAFTestStep(description ="Submit button Functionality to mark attendance by clicking on absent present dot")
    public void TC_Class_BatchWiseAttene_14(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkClass.click();
		Reporter.log("User Clicked on Class Link");
		FullEPrashasanMarkAttendancePage fullEPrashasanMarkAttendancePage=new FullEPrashasanMarkAttendancePage();
//		fullEPrashasanMarkAttendancePage.waitForPageToLoad();

		fullEPrashasanMarkAttendancePage.topNavigationComponent.linkAttendance.click();
		Reporter.log("User Clicked on Attendance Link");
		fullEPrashasanMarkAttendancePage.linkBatchWiseAttendance.click();
		Reporter.log("User Clicked on Batch Wise Attendance link");
		
		FullEPrashasanBatchAttendancePage fullEPrashasanBatchAttendancePage=new FullEPrashasanBatchAttendancePage();
		fullEPrashasanBatchAttendancePage.dropdownAcademicYear.sendKeys("2019-2020");
		fullEPrashasanBatchAttendancePage.dropdownCourse.selectByOptionValue("-English");
		fullEPrashasanBatchAttendancePage.linkBatch1.click();
		
		fullEPrashasanBatchAttendancePage.linkMarkAttendance.click();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.waitForPopupToLoad();
	
    	fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonPresent.click();
    	fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.labelPresentAbsent.click();
		
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonSubmit.click();
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Data Saved Successfully"),
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		fullEPrashasanBatchAttendancePage.popupMarkBatchWiseAttendance.buttonClose.click();
		
	}


}
