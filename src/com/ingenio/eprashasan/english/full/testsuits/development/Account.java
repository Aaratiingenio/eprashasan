package com.ingenio.eprashasan.english.full.testsuits.development;

import java.util.Map;

import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import com.ingenio.eprashasan.core.EPUtils;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAccountLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAccountPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAccountChangePasswordPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAccountCreateSchoolCollegeRegistration;
import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAccountVoucherEntry1Page;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class Account extends WebDriverTestCase{
	
		@Test (priority=1)
		@QAFDataProvider(key="login.data")
		@MetaData(value = "{'groups':['Account Login Functionaltiy']}")
		@QAFTestStep(description = "Validate the Account login functionality")
		public void TC_AccountLogin_01(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAccount.click();
		Reporter.log("User Clicked on Account Link");
		FullEPrashasanAccountPage fullEPrashasanAccountPage =new FullEPrashasanAccountPage();
		fullEPrashasanAccountPage.linkAccountLogin.click();
		
		FullEPrashasanAccountLoginPage fullEPrashasanAccountLoginPage =new FullEPrashasanAccountLoginPage();
		fullEPrashasanAccountLoginPage.selectSchoolName.selectByOptionValue(data.get("Account_Login1"));
		fullEPrashasanAccountLoginPage.textboxUsername.sendKeys(data.get("Account_Login1_Username"));
		fullEPrashasanAccountLoginPage.textboxPassword.sendKeys(data.get("password"));
		fullEPrashasanAccountLoginPage.buttonLogin.submit();
		
		FullEPrashasanAccountVoucherEntry1Page fullEPrashasanVoucherEntry1Page = new FullEPrashasanAccountVoucherEntry1Page();
		fullEPrashasanVoucherEntry1Page.headerVoucherEntry.verifyText("Voucher Entry");
		
}
		@Test (priority=2)
		@QAFDataProvider(key="login.data")
		@MetaData(value = "{'groups':['Create School College Registration']}")
		@QAFTestStep(description = "Validate the Create School College Registration")
		public void TC_AccountCreateScoolReg_02(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAccount.click();
		Reporter.log("User Clicked on Account Link");
		FullEPrashasanAccountPage fullEPrashasanAccountPage =new FullEPrashasanAccountPage();
		fullEPrashasanAccountPage.linkCreateSchoolCollegeRegistration.click();
		
		FullEPrashasanAccountCreateSchoolCollegeRegistration fullEPrashasanCreateSchoolCollegeRegistration = new FullEPrashasanAccountCreateSchoolCollegeRegistration();
		fullEPrashasanCreateSchoolCollegeRegistration.selectOrgType.selectByOptionValue("School");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxSchoolName.sendKeys("TodayDate()");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxRegNo.sendKeys("123");
		fullEPrashasanCreateSchoolCollegeRegistration.textareaAddress.sendKeys("Baner,Pune");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxPanNo.sendKeys("SDER1234566");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxVatNo.sendKeys("986532");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxTanNo.sendKeys("98653214");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxServiceTaxNo.sendKeys("587998");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxCstNo.sendKeys("4562");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxUsername.sendKeys("abc");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxPassword.sendKeys("abc");
		fullEPrashasanCreateSchoolCollegeRegistration.textboxRetypePassword.sendKeys("abc");
		fullEPrashasanCreateSchoolCollegeRegistration.buttonAdd.click();
}
		@Test (priority=3)
		@QAFDataProvider(key="login.data")
		@MetaData(value = "{'groups':['Change Password Funcionality']}")
		@QAFTestStep(description = "Validate the Change Password Funcionality")
		public void TC_AccountChangePassword_03(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAccount.click();
		Reporter.log("User Clicked on Account Link");
		FullEPrashasanAccountPage fullEPrashasanAccountPage =new FullEPrashasanAccountPage();
		fullEPrashasanAccountPage.linkChangePassword.click();
		
		FullEPrashasanAccountChangePasswordPage fullEPrashasanChangePasswordPage = new FullEPrashasanAccountChangePasswordPage();
		fullEPrashasanChangePasswordPage.selectSchoolName.selectByOptionValue("Account-Student Wise");
		fullEPrashasanChangePasswordPage.textboxUserName.sendKeys("sadmin");
		fullEPrashasanChangePasswordPage.textboxPassword.sendKeys("admin");
		fullEPrashasanChangePasswordPage.textboxNewPassword.sendKeys("admin1");
}
		@Test (priority=4)
		@QAFDataProvider(key="login.data")
		@MetaData(value = "{'groups':['Edit Create School College Registration']}")
		@QAFTestStep(description = "Update School Registration fields")
		public void TC_AccountEditSchoolReg_04(Map<String, String> data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
			
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAccount.click();
		Reporter.log("User Clicked on Account Link");
		FullEPrashasanAccountPage fullEPrashasanAccountPage =new FullEPrashasanAccountPage();
		fullEPrashasanAccountPage.linkCreateSchoolCollegeRegistration.click();
		FullEPrashasanAccountCreateSchoolCollegeRegistration fullEPrashasanCreateSchoolCollegeRegistration = new FullEPrashasanAccountCreateSchoolCollegeRegistration();
		fullEPrashasanCreateSchoolCollegeRegistration.imgEdit.click();
		Thread.sleep(2000);
}
		@Test (priority=5)
		@QAFDataProvider(key="login.data")
		@MetaData(value = "{'groups':['Delete Create School College Registration']}")
		@QAFTestStep(description = "Delete School Registration")
		public void TC_AccountDeleteSchoolReg_05(Map<String, String> data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
			
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAccount.click();
		Reporter.log("User Clicked on Account Link");
		FullEPrashasanAccountPage fullEPrashasanAccountPage =new FullEPrashasanAccountPage();
	    fullEPrashasanAccountPage.linkCreateSchoolCollegeRegistration.click();
		FullEPrashasanAccountCreateSchoolCollegeRegistration fullEPrashasanCreateSchoolCollegeRegistration = new FullEPrashasanAccountCreateSchoolCollegeRegistration();
		Thread.sleep(2000);
		fullEPrashasanCreateSchoolCollegeRegistration.imgDelete.click();
		getDriver().switchTo().alert().accept();
		Thread.sleep(2000);
}
		@Test (priority=6)
		@QAFDataProvider(key="login.data")
		@MetaData(value = "{'groups':['NEW VOucher Entry']}")
		@QAFTestStep(description = "New Voucher Entry should be added")
		public void TC_AccountVoucherEntry1_06(Map<String, String> data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
			
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkAccount.click();
		Reporter.log("User Clicked on Account Link");
		FullEPrashasanAccountPage fullEPrashasanAccountPage =new FullEPrashasanAccountPage();
		fullEPrashasanAccountPage.linkAccountLogin.click();
			
		FullEPrashasanAccountLoginPage fullEPrashasanAccountLoginPage =new FullEPrashasanAccountLoginPage();
		fullEPrashasanAccountLoginPage.selectSchoolName.selectByOptionValue("Account-Student Wise");
		fullEPrashasanAccountLoginPage.textboxUsername.sendKeys("sadmin");
		fullEPrashasanAccountLoginPage.textboxPassword.sendKeys("admin");
		fullEPrashasanAccountLoginPage.buttonLogin.submit();
			
		fullEPrashasanAccountPage.linkVoucherEntry.click();
		fullEPrashasanAccountPage.linkVoucherEntry1.click();
			
		FullEPrashasanAccountVoucherEntry1Page fullEPrashasanVoucherEntry1Page = new FullEPrashasanAccountVoucherEntry1Page();
		fullEPrashasanVoucherEntry1Page.imgNewEntry.click();
		fullEPrashasanVoucherEntry1Page.selectDate.selectByOptionValue("TodayDate()");
		fullEPrashasanVoucherEntry1Page.selectMonth.selectByOptionValue("TodayDate()");
		fullEPrashasanVoucherEntry1Page.selectYear.selectByOptionValue("TodayDate()");
		fullEPrashasanVoucherEntry1Page.selectVoucherType.selectByIndex(1);
		fullEPrashasanVoucherEntry1Page.selectDebitLedger.selectByIndex(14);
		fullEPrashasanVoucherEntry1Page.selectCreditLedger.selectByIndex(2);
		fullEPrashasanVoucherEntry1Page.textboxNarration.sendKeys("Account no=123655");
		fullEPrashasanVoucherEntry1Page.textboxPaymentDetails.sendKeys("To Bank Of India");
		fullEPrashasanVoucherEntry1Page.textboxAmount.sendKeys("5000");
		fullEPrashasanVoucherEntry1Page.textboxAmount.sendKeys(Keys.ENTER);
		Thread.sleep(20000);
}
		
	 
}