package com.ingenio.eprashasan.english.full.testsuits.development;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertSame;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.ingenio.eprashasan.full.pages.FullEPrashasanQuickRegistrationFormPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.util.Reporter;

public class GetSQLData {
	
	static Integer schoolId=0;
	static Integer sansthaId=0;
	
	@QAFDataProvider(key="sanstharegistration.data")
	
/* Script to Verify Added Sanstha Data */
	
	public static void getSansthaRegistartion(Map<String, String> data)
	{	
		Reporter.log("Running Get Sanstha Registration Data Script");
    	Connection con = null;
    	boolean isRecordInserted=false;
    	try {
	
    		
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "testing", "ng#testing121");
		Statement smt=con.createStatement();
		ResultSet rs= smt.executeQuery("select a.sansthaKey from sanstha_registration a where a.sansthaKey="+data.get("sansthakey"));
		while(rs.next()){
			isRecordInserted=true;
			
		    sansthaId= rs.getInt("sansthaId");
//			Integer APPUSERROLEID=rs.getInt("APPUSERROLEID");
//			Integer USERNAME=rs.getInt("USERNAME");
			Reporter.log("Sanstha ID is : "+sansthaId);
			break;
			}
		assertEquals(true,isRecordInserted,"No Record Inserted");
		} catch (Exception e) {
			Reporter.log("Failed to execute delete data");
			System.err.println("Failed to Execute");
		} 
	}
	
	
	/* Script to Verify Added School Data ***/
	
	public static void getSchoolMaster(Map<String, String> data)  {	
		Reporter.log("Running Get School Master Data Script");
    	Connection con = null;
    	boolean isRecordInserted=false;
    	try {

		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "testing","ng#testing121");
		Statement smt=con.createStatement();
		ResultSet rs= smt.executeQuery("select a.schoolid from school_master a where a.schoolKey="+data.get("schoolKey"));
		while(rs.next()){
			isRecordInserted=true;
			schoolId= rs.getInt("schoolid");
			
			Reporter.log("School ID is : "+schoolId);
			break;
			}
		assertEquals(true,isRecordInserted,"No Record Inserted");
		} catch (Exception e) {
			Reporter.log("Failed to execute delete data");
			System.err.println("Failed to Execute");
		} 
	}
	   
	
	
	
	public static void getStudentMaster(Map<String, String> data)  {	
		Reporter.log("Running Get Student Master Data Script");
    	Connection con = null;
    	boolean isRecordInserted=false;
    	try {
    		/*FullEPrashasanQuickRegistrationFormPage fullEPrashasanQuickRegistrationFormPage = new FullEPrashasanQuickRegistrationFormPage();
    		fullEPrashasanQuickRegistrationFormPage.waitForPageToLoad();
    		fullEPrashasanQuickRegistrationFormPage.officeDetailsComponent.textboxFirstName.sendKeys(data.get("firstName"));*/
    		
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "eprashasan", "Root@123");
			Statement smt=con.createStatement();
			String studentMasterQuery = "Select a.stud_id,a.schoolid,a.Student_RegNo,a.studFName,a.studMName,a.studLName,"
					+ "b.renewstudentId,b.renewAdmissiondate,b.alreadyRenew,c.textbox1 from student_master a "
					+ "left join student_standard_renew b on a.stud_id=b.studentId "
					+ "left join dynamicfieldstudinformation c on c.studId=a.stud_id "
					+ "where a.schoolid='"+schoolId+"' and a.Student_RegNo='"+data.get("alphanumericRegNo")+"'";
		ResultSet rs= smt.executeQuery(studentMasterQuery);
		while(rs.next()){
			isRecordInserted=true;
			Integer studentId= rs.getInt("stud_id");
			String schoolid= rs.getString("schoolid");
			String StudentRegNo=  rs.getString("Student_RegNo");
			String studFName=  rs.getString("studFName");
			String studMName=  rs.getString("studMName");
			String studLName=  rs.getString("studLName");
			
			Reporter.log("Student id is "+studentId+" school id is "+schoolid+" Student_RegNo id is "+StudentRegNo);
			
			assertEquals((""+studentId).length(),10);
			assertEquals((""+studentId).substring(0, 2),("1010000001").substring(0, 2));
			assertEquals(schoolid,"1010000001");
			assertEquals(data.get("alphanumericRegNo"),StudentRegNo,"Reg No mismatch");
			assertEquals(data.get("firstName"),studFName);
			assertEquals(data.get("middleName"),studMName);
			assertEquals(data.get("lastName"),studLName);
			
				break;
	
					}
					assertEquals(true,isRecordInserted,"No Record Inserted");
				
			} catch (Exception e) {
				Reporter.log("Failed to execute delete data");
				System.err.println("Failed to Execute");
			} 
		}
		    
		    // Staff Quick Admission Verification 
		   
	    	@QAFDataProvider(key="StaffQuickAdmission.data")	   
		    public static void getstaffQuickAdmissionQuery(Map<String, String> data)  {	
		   
				Reporter.log("Running Get StaffQuickAdmission Data Script");
		    	Connection con = null;
		    	
	    	boolean isRecordInserted=false;
	    	try {
				
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "testing", "ng#testing121");
			Statement smt=con.createStatement();
			String staffQuickAdmissionQuery =
			"select a.sreg_no,a.staaffId,b.FIRST_NAME,b.LAST_NAME,b.USERNAME,a.schoolid,a.staaffId,a.yearId from staff_basicdetails a"  
			+"left join app_user b on a.sreg_no=b.USERNAME" 
			+"where a.FLG_QUICK_ADMISSION=1 and a.firstname='"+data.get("firstname")+"'";
				
			ResultSet rs= smt.executeQuery(staffQuickAdmissionQuery);
			while(rs.next()){
				isRecordInserted=true;
			
			String staffId= rs.getString("staaffId");
			String SchoolId=rs.getString("schoolid");
		    String yearId= rs.getString("yearId");
		    String firstName= rs.getString("FIRST_NAME");
		    String lastName= rs.getString("LAST_NAME");
		    String userName= rs.getString("USERNAME");
		    String regNo= rs.getString("sreg_no");
			    
			Reporter.log("Staff id is "+staffId+" school id is "+SchoolId+" YearId is "+yearId+" FirstName is "+firstName+" LastName is "+lastName+" UserName is "+userName+" RegNo is "+regNo);
			
			assertEquals((""+staffId).length(),10);
			assertEquals((""+regNo).substring(0,2),("1011200001").substring(0,2));
			assertEquals(SchoolId,schoolId,"SchoolId Mismatch");
			assertEquals(data.get("firstname"),firstName,"First Name Mismatch");
			
		    break;
					}
					assertEquals(true,isRecordInserted,"No Record Inserted");
				
			} catch (Exception e) {
				Reporter.log("Failed to execute delete data");
				System.err.println("Failed to Execute");
			} 
	    }
		  
		     //Assign Subject to staff Verification
		
		
	    	public static void getstaff_subjectdetails(Map<String, String> data)  {	
			
			Reporter.log("Running Get staff_subjectdetails Data Script");
	    	Connection con = null;
	    	boolean isRecordInserted=false;
	    	try {
	    /*		FullEPrashasanAssignSubjectToStaff fullEPrashasanAssignSubjectToStaff = new FullEPrashasanAssignSubjectToStaff();
	    		FullEPrashasanAssignSubjectToStaff.waitForPageToLoad();
	    		FullEPrashasanAssignSubjectToStaff.dropdownYear.selectByIndex(2);*/
	
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "eprashasan", "Root@123");
			Statement smt=con.createStatement();
		
		
	
			String staff_subjectdetailsQuery =
			"select a.staffSubId,a.schoolid,a.yearId,a.stdId,a.divId,a.subjectId ,a.staffId,b.firstname,b.lastname,c.subject_name from\r\n"  
			+ "staff_subjectdetails a\r\n" 
			+"left join staff_basicdetails b on a.staffId=b.staaffId\r\n"  
			+"left join subjects c on a.subjectId=c.subject_id\r\n"  
			+"where a.staffId='1011100002'";
				
		ResultSet rs= smt.executeQuery(staff_subjectdetailsQuery);
		while(rs.next()){
			
			isRecordInserted=true;
			Integer staffSubId= rs.getInt("staffSubId");
			String staffId= rs.getString("staffId");
			String SchoolId=rs.getString("schoolid");
			String subjectId= rs.getString("subjectId");
			String yearId= rs.getString("yearId");
			String standardId= rs.getString("stdId");
			String divisionId= rs.getString("divId");
			String firstName= rs.getString("firstname");
			Reporter.log("Staff id is "+staffId+" school id is "+schoolId);
			
			assertEquals((""+staffSubId).length(),10);
			assertEquals((""+staffSubId).substring(0, 2),("1000100001").substring(0, 2));
			
			
			break;
	
				}
				assertEquals(true,isRecordInserted,"No Record Inserted");
			
				} catch (Exception e) {
			Reporter.log("Failed to execute View data");
			System.err.println("Failed to Execute");
		} 
	}
	    	
	    	
    	public static void getTableCount(Map<String, String> data)  {
			Reporter.log("Running Get table Master Data Script");
	    	Connection con = null;
	    	boolean isRecordInserted=false;
	    	try {
//		    				    		
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "testing","ng#testing121");
			Statement smt=con.createStatement();
			
		String[] tableNameArr = new String[] {"absent_student_msg","acc_account_login","acc_create_school_college","advertisement_master","agesetting","birthdaymessage",
				"block_month_make_visible","bonafiedheaderfooter", "bonafiedprioritysavedvalues","bonafiedprioritysetting","bonafied_linking_master","category",
				"concession_master","co_scholastic_master_type","dss_activation","dynamicadmissionfileds","dynamic_acad_months","fees_receipt_setting","fee_pay_type",
				"fee_receiptheader","fee_send_emailids_mstr","fee_send_sms_numbers_mstr","gradetable","lib_currency_master","lib_isbn_access_config","minority_master",
				"present_student_msg","religion","religion_master_1","religion_master_2","religion_master_3","reminder_close_popup","subject_patterns","subject_type",
				"subject_type_master","supadtab","super_admin_emailids_mstr","tcheaderfooter","tcprioritysavedvalues","tcprioritysetting","tc_linking_master","pr_leave_policy",
				"pr_pages","pr_salary_bank_copy_setting","pr_sandwich_leave_setting","fee_dynamic_challan_setting","stck_issue_purchase_order_setting","gr_book_name","stck_tax",
				"android_menu","attendance_labels","fee_sms_mail_setting","school_master","android_home_message_category","android_home_screen_master","help_module",
				"help_submodule","stck_login","trans_fee_pay_type","staff_handicaptype","department","designation","staff_paymenttype","staff_schooltype",
				"staff_type","design_admi_form_tabs","design_admi_form_legend","design_admi_form_fields","attendance_num_of_time","reportmaster","attendance_report_setting"};
				                              
		int[] expectedRowCountArr= new int[] {4,3,3,1,1,2,1,1,25,61,1,10,12,7,10,10,8,8,12,1,1,1,6,16,1,8,4,11,8,8,8,2,1,1,1,1,2,1,25,65,1,3,
				22,1,3,33,1,1,3,21,5,4,1,1,7,37,493,1,9,1,1,2,1,1,1,5,1,45,1,3,6};
		
		for(int i=0;i<tableNameArr.length;i++) {
			isRecordInserted = checkRowCount(isRecordInserted, smt, tableNameArr[i], expectedRowCountArr[i]);
		}
		
			
	/* String defaultentryMasterQuery1="select * from principal_menu_master";
			ResultSet rs2= smt.executeQuery(defaultentryMasterQuery1);
			while(rs2.next()){
				isRecordInserted=true;
				rs2.last();
				Reporter.log("RowCount in  principal_menu_master table : "+rs2.getRow());
				
				
				assertEquals(rs2.getRow(),11,"No.of Rows and Expected Count Mismatched");
				break;
			}
			
			String defaultentryMasterQuery3="select * from acc_create_school_college a  where a.schoolid='"+schoolId+"'";
			ResultSet rs3= smt.executeQuery(defaultentryMasterQuery3);
			while(rs3.next()){
				isRecordInserted=true;
				rs3.last();
				Reporter.log("RowCount in acc_create_school_college table : "+rs3.getRow());
				
				
				assertEquals(rs3.getRow(),3,"No.of Rows and Expected Count Mismatched");
				break;
			}*/
			
			assertEquals(true,isRecordInserted,"No Record Inserted");
			} catch (Exception e) {
				Reporter.log("Failed to execute delete data");
				System.err.println("Failed to Execute");
			} 
		}


		private static boolean checkRowCount(boolean isRecordInserted, Statement smt, String tableName,
				int expectedRowCount) throws SQLException {
			String defaultentryMasterQuery1="select * from "+tableName+" a  where a.schoolid='"+schoolId+"'";
			ResultSet rs1= smt.executeQuery(defaultentryMasterQuery1);
			while(rs1.next()){
				isRecordInserted=true;
				rs1.last();
				Reporter.log("RowCount in  " +tableName+ " Table : "+rs1.getRow());
			
				assertEquals(rs1.getRow(),expectedRowCount,"No.of Rows and Expected Count Mismatched");
				break;
			}
			return isRecordInserted;
		}
	    	
	    	
	    	}
		
	
		
		
		
	   