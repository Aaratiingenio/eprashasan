package com.ingenio.eprashasan.english.full.testsuits.development;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

import com.ibatis.common.jdbc.ScriptRunner;
import com.mysql.jdbc.Statement;
import com.qmetry.qaf.automation.util.Reporter;

public class RunSqlScript {
	
	
	
	/* Script To Delete added Sanstha ***/
	
	public static void executeQueryBySqlFile() throws ClassNotFoundException, SQLException, FileNotFoundException {	
		Reporter.log("Running delete script");
        BufferedReader reader = null;
    	Connection con = null;
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://15.206.112.139:3306/dbe1?characterEncoding/=UTF-8", "testing", "ng#testing121");
		ScriptRunner sr = new ScriptRunner(con, false, false);
		try {
			reader = new BufferedReader(new FileReader("D:\\Automation Workspace\\seleniumeprashasanautomation\\resources\\delete_data.sql"));
			sr.runScript(reader);
			Reporter.log("Delete Completed");
			
		} catch (Exception e) {
			Reporter.log("Failed to execute delete data");
			System.err.println("Failed to Execute");
		} finally {
			// close file reader
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// close db connection
			if (con != null) {
				con.close();
			}
		}
	}
	
	
	/* Script To Delete added School ***/
	
	public static void DeleteDatabase() {
		System.out.println("Start Date="+new Date());
		String destinationDB = "dbe1";
		String destinationDBIP="15.206.112.139:3306";
		String schoolId="1011600001";
		String dbUser="testing";
		String dbPass="ng#testing121";
		String destinationDBdbURl="jdbc:mysql://"+destinationDBIP+"/"+destinationDB+"?characterEncoding/=UTF-8";
		Connection destinationDBconn = null;
		java.sql.Statement destinationDBstmt = null;
		try{						
			Class.forName("com.mysql.jdbc.Driver");		
			destinationDBconn = DriverManager.getConnection(destinationDBdbURl,dbUser,dbPass);
			destinationDBconn.setAutoCommit(false);
			destinationDBstmt = destinationDBconn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);	
			
			destinationDBstmt.executeUpdate("SET FOREIGN_KEY_CHECKS=0;"); 
			
			String egnorSchoolIdColumnNoAvailableTables[]={"android_menu_master","android_configuration","app_user_exam_class","acc_create_ledger_mapp","app_language","app_role","book_add_cart","dynamiccombomaster","exam","exam_name_with_exams"
					,"exam_struct_with_exam_name","exam_stud_hobbies","exam_stud_progress","exam_stud_remark","exam_stud_required_progress","exams_name","fee_sms_mail_history","fee_to_acc_link","globaldb","item_add_cart","jan_attendance","jr_exam"
					,"jr_examtimetable","jr_gradetable","jr_result","jr_subject_master","librarydueamt","library_book_available","library_book_type","library_class","library_clearence","library_due_amount_setting","library_newspaper_info","library_newspaper_transactions"
					,"library_no_of_books_to_be_issued","library_stream","librarydueamt","marksheet_excel","reports_setting","school_data_sinking_setting","school_type","sec_examtimetable"
					,"secondary_exam","secondary_gradetable","secondary_result","secondary_subject_master","skiped_alias","sms_template_master","standard","static_fields","subject_master","subject_master_temp","subject_type_category","subject_with_type",
					"trans_student_bus_detail","acc_create_ledger_head_mapp","android_desk_summary_master","group_of_school","principal_menu_master","sms_api_mstr_for_otp","android_notification_master","school_board","lib_sms_setting"};
			
			String sansthaTables[]={"sanstha_registration","sanstha_user_registration","sanstha_for_challan_bank_details","sanstha_user_mapp_to_school"};
			
			String[] types = {"TABLE"};
			ResultSet resultSetTables = destinationDBconn.getMetaData().getTables(destinationDB, null, "%", types); //get All Tables
			String tableName = "";			
			while (resultSetTables.next()) {				
				tableName = resultSetTables.getString(3);
				if(!Arrays.asList(egnorSchoolIdColumnNoAvailableTables).contains(tableName)) {
					String deleStr="DELETE FROM `"+destinationDB+"`.`"+tableName+"` WHERE";
					if(tableName.equalsIgnoreCase("trans_standard")) {
						deleStr+=" `schoolid1`='"+schoolId+"'";
					}else if (Arrays.asList(sansthaTables).contains(tableName)) {
						deleStr+=" `sansthaId`='"+schoolId+"'";
					} else {
						deleStr+=" 'schoolid`='"+schoolId+"'";
					}
					System.out.println(deleStr);					
					destinationDBstmt.executeUpdate(deleStr);  //FOR REPLACE
					destinationDBconn.commit();		
				}
			}			
			destinationDBstmt.executeUpdate("SET FOREIGN_KEY_CHECKS=1;");				
			destinationDBconn.commit();			
			resultSetTables.close();
			destinationDBstmt.close();	
			System.out.println("Success Commited Data.");
		}catch(SQLException se){
			se.printStackTrace();
			System.out.println("Rolling Back Data.");
			try{				
				if(destinationDBconn!=null)
					destinationDBconn.rollback();
			}catch(SQLException se2){
				se2.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{				
				if(destinationDBstmt!=null)
					destinationDBstmt.close();
			}catch(SQLException se2){
				se2.printStackTrace();
			}
			try{
				if(destinationDBconn!=null)
					destinationDBconn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}
			System.out.println("End Date="+new Date());
		}
		System.out.println("Finnish....!");
	}
	
	
   }