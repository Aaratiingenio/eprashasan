package com.ingenio.eprashasan.english.full.testsuits.development;

import java.util.Map;

import org.openqa.selenium.Alert;
import org.testng.annotations.Test;


import com.ingenio.eprashasan.core.EPUtils;
//import com.ingenio.eprashasan.Full.Pages.EPrashasanSchoolRegistrationFormPage;
import com.ingenio.eprashasan.core.EPUtils;

import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;

import com.ingenio.eprashasan.full.pages.FullSansthaRegistrationHomePage;
import com.ingenio.eprashasan.full.pages.FullSchoolRegistrationHomePage;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.util.Reporter;

public class SchoolRegistration extends WebDriverTestCase{

	@Test
	//@QAFDataProvider(key="login.data")
	@MetaData(value = "{'groups':['Login Functionaltiy']}")
	@QAFTestStep(description = "Validate the login functionality with invalid credintial")
	public void TC_School_Reg_01() throws InterruptedException {
	
		FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
		fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
		fullSchoolRegistrationHomePage.waitForPageToLoad();
		//Utils.sleep(5000);
		
		
		fullSchoolRegistrationHomePage.buttonSubmit.click();
		Reporter.log("User Clicked on Submit Button");
		EPUtils.sleep(3000);
		
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Please Enter School Name"), 
						"Alert message not displayed",
						"Alert message displayed");
		getDriver().switchTo().alert().accept();
		
		
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without entering \"School Reg No\"")
	public void TC_School_Reg_02(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter Reg No"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
							
	}
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without entering \"School Key\"")
	public void TC_School_Reg_03(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter School Key"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
							
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Selecting \"Language\"")
	public void TC_School_Reg_04(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Select Language"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
							
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Selecting \"School Type\"")
	public void TC_School_Reg_05(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Select School Type"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Selecting \"School Board\"")
	public void TC_School_Reg_06(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Select School Board"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Entering \"First Name\"")
	public void TC_School_Reg_07(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter First Name"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Entering \"Last Name\"")
	public void TC_School_Reg_08(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter Last Name"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Entering \"Username\"(Mobile No)")
	public void TC_School_Reg_09(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter User Name"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without Entering \"Password\"")
	public void TC_School_Reg_10(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter Password"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without selecting Question 1")
	public void TC_School_Reg_11(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
				fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Select Question 1"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without  entering the Answer for Question 1")
	public void TC_School_Reg_12(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
				fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
				fullSchoolRegistrationHomePage.dropdownQuestion1.selectByIndex(1);
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter Answer 1"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without selecting Question 2")
	public void TC_School_Reg_13(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
				fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
				fullSchoolRegistrationHomePage.dropdownQuestion1.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxAnswer1.sendKeys(data.get("answer1"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Select Question 2"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  without  entering the Answer for Question 2")
	public void TC_School_Reg_14(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
				fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
				fullSchoolRegistrationHomePage.dropdownQuestion1.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxAnswer1.sendKeys(data.get("answer1"));
				fullSchoolRegistrationHomePage.dropdownQuestion2.selectByIndex(1);
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please Enter Answer 2"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by Entering LESS THAN 10 DIGITS \"Username\"(Mobile No)")
	public void TC_School_Reg_15(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("invalidUsername1"));
				fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
				fullSchoolRegistrationHomePage.dropdownQuestion1.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxAnswer1.sendKeys(data.get("answer1"));
				fullSchoolRegistrationHomePage.dropdownQuestion2.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxAnswer2.sendKeys(data.get("answer2"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("Please enter valid mobile no for user name"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
	}
	
	
	
	// English School Registration with actual data 
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
	@QAFTestStep(description = "Validate \"Submit\" button functionality  by Entering MORE THAN 10 DIGITS \"Username\"(Mobile No)")
	public void TC_School_Reg_16(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				//Utils.sleep(5000);
				
				fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
				fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
				fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
				fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
				fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
				fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
				fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
				fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(1);
				fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
				EPUtils.sleep(2000);
				fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
				fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
				fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
				fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
				fullSchoolRegistrationHomePage.dropdownQuestion1.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxAnswer1.sendKeys(data.get("answer1"));
				fullSchoolRegistrationHomePage.dropdownQuestion2.selectByIndex(1);
				fullSchoolRegistrationHomePage.textboxAnswer2.sendKeys(data.get("answer2"));
				fullSchoolRegistrationHomePage.buttonSubmit.click();
				Reporter.log("User Clicked on Submit Button");
				EPUtils.sleep(3000);
				
				
				Alert Windowalert = getDriver().switchTo().alert();
				
				verifyTrue(Windowalert.getText().contains("School Added Successfully"),
								" Alert message displayed",
								" Alert message not displayed");
				getDriver().switchTo().alert().accept();
				
//				GetSQLData.getSchoolMaster(data);
				
//				GetSQLData.getTableCount(data);
				
	}
	
	
	
	
	
	@Test
	@QAFDataProvider(key="schoolregistration.data")
	@MetaData(value = "{'groups':['SchoolRegistration']}")
    @QAFTestStep(description = "Verify Added School By Logging in using AlreadyRegisteredLink")

	public void TC_School_Reg_17(Map<String, String>data) {
	
				FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
				fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
				fullSchoolRegistrationHomePage.waitForPageToLoad();
				EPUtils.sleep(2000);
				
				fullSchoolRegistrationHomePage.linkAlreadyRegistered.click();



				EPUtils.sleep(3000);
				
				FullEPrashasanLoginPage fullEPrashasanLoginPage =new FullEPrashasanLoginPage();
				fullEPrashasanLoginPage.textboxUsername.sendKeys(data.get("userName"));
				Reporter.log("User Entered Username");
				fullEPrashasanLoginPage.textboxPassword.sendKeys(data.get("password"));
				Reporter.log("User Entered Password");
				fullEPrashasanLoginPage.buttonLogin.click();
				Reporter.log("User Clicked On Login Button");
				
						
				
				FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
				fullEPrashasanWelcomePage.waitForPageToLoad();
				verifyTrue(fullEPrashasanWelcomePage.headerSchoolName.isPresent(),
			 
				"User not navigated to Welcome Page", "User navigated to Welcome Page");
				
     			GetSQLData.getSchoolMaster(data);
				GetSQLData.getTableCount(data);
			
				
//				
//				RunSqlScript runSqlScript = new RunSqlScript();
//     			RunSqlScript.DeleteDatabase();
				
	}
	
	
	
	// Marathi School Registration with actual data 
		
	    @Test
		@QAFDataProvider(key="marathischoolregistration.data")
		@MetaData(value = "{'groups':['MarathiSchoolRegistration']}")
		@QAFTestStep(description = "Validate \"Submit\" button functionality  by Entering MORE THAN 10 DIGITS \"Username\"(Mobile No)")
		public void TC_School_Reg_18(Map<String, String>data) {
		
					FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
					fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
					fullSchoolRegistrationHomePage.waitForPageToLoad();
					//Utils.sleep(5000);
					
					fullSchoolRegistrationHomePage.textboxSchoolName.sendKeys(data.get("schoolName"));
					fullSchoolRegistrationHomePage.textboxSansthaName.sendKeys(data.get("sansthaName"));
					fullSchoolRegistrationHomePage.textboxSansthaRegNo.sendKeys(data.get("sansthaRegNo"));
					fullSchoolRegistrationHomePage.textareaSchoolAddress.sendKeys(data.get("schoolAddress"));
					fullSchoolRegistrationHomePage.textboxSchoolRegNo.sendKeys(data.get("schoolRegNo"));
					fullSchoolRegistrationHomePage.textboxSansthaKey.sendKeys(data.get("sansthaKey"));
					fullSchoolRegistrationHomePage.textboxSchoolKey.sendKeys(data.get("schoolKey"));
					fullSchoolRegistrationHomePage.dropdownLanguage.selectByIndex(2);
					fullSchoolRegistrationHomePage.dropdownSchoolType.selectByIndex(1);
					EPUtils.sleep(2000);
					fullSchoolRegistrationHomePage.dropdownSchoolBoard.selectByIndex(1);
					fullSchoolRegistrationHomePage.textboxFirstName.sendKeys(data.get("firstName"));
					fullSchoolRegistrationHomePage.textboxLastName.sendKeys(data.get("lastName"));
					fullSchoolRegistrationHomePage.textboxUserName.sendKeys(data.get("userName"));
					fullSchoolRegistrationHomePage.textboxPassword.sendKeys(data.get("password"));
					fullSchoolRegistrationHomePage.dropdownQuestion1.selectByIndex(1);
					fullSchoolRegistrationHomePage.textboxAnswer1.sendKeys(data.get("answer1"));
					fullSchoolRegistrationHomePage.dropdownQuestion2.selectByIndex(1);
					fullSchoolRegistrationHomePage.textboxAnswer2.sendKeys(data.get("answer2"));
					fullSchoolRegistrationHomePage.buttonSubmit.click();
					Reporter.log("User Clicked on Submit Button");
					EPUtils.sleep(1000);
					
					
					Alert Windowalert = getDriver().switchTo().alert();
					
					verifyTrue(Windowalert.getText().contains("School Added Successfully"),
									"School Added Successfully Alert message displayed",
									"School Added Successfully Alert message not displayed");
					getDriver().switchTo().alert().accept();
					
//					GetSQLData.getSchoolMaster(data);
					
//					GetSQLData.getTableCount(data);
					
		}
	    
	    
	    //Verify tables for Marathi School Registration
	    
	    @Test
		@QAFDataProvider(key="marathischoolregistration.data")
		@MetaData(value = "{'groups':['MarathiSchoolRegistration']}")
	    @QAFTestStep(description = "Verify Added School By Logging in using AlreadyRegisteredLink")

		public void TC_School_Reg_19(Map<String, String>data) {
		
			FullSchoolRegistrationHomePage fullSchoolRegistrationHomePage = new FullSchoolRegistrationHomePage();
			fullSchoolRegistrationHomePage.invoke("/error/openSchoolRegistrationPage.do");
			fullSchoolRegistrationHomePage.waitForPageToLoad();
			EPUtils.sleep(2000);
			
			fullSchoolRegistrationHomePage.linkAlreadyRegistered.click();

			EPUtils.sleep(3000);
			
			FullEPrashasanLoginPage fullEPrashasanLoginPage =new FullEPrashasanLoginPage();
			fullEPrashasanLoginPage.textboxUsername.sendKeys(data.get("userName"));
			Reporter.log("User Entered Username");
			fullEPrashasanLoginPage.textboxPassword.sendKeys(data.get("password"));
			Reporter.log("User Entered Password");
			fullEPrashasanLoginPage.buttonLogin.click();
			Reporter.log("User Clicked On Login Button");
			
			
			FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
			fullEPrashasanWelcomePage.waitForPageToLoad();
			
			verifyTrue(fullEPrashasanWelcomePage.headerSchoolName.isPresent(),
		    "User not navigated to Welcome Page", "User navigated to Welcome Page");
			
 			GetSQLData.getSchoolMaster(data);
			GetSQLData.getTableCount(data);
		
			
			
			//RunSqlScript runSqlScript = new RunSqlScript();
 			//RunSqlScript.DeleteDatabase();
					
		}
}
