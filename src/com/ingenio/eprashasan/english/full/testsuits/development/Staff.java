package com.ingenio.eprashasan.english.full.testsuits.development;

import java.io.IOException;
import java.util.Map;
import static org.testng.Assert.assertEquals;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.ingenio.eprashasan.core.EPUtils;
import com.ingenio.eprashasan.full.pages.FullEPrashasanAssignSubjectToStaff;
import com.ingenio.eprashasan.full.pages.FullEPrashasanExcelImportPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanLoginPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffBasicDetailsFormPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffDepartmentPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffDesignationPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffQuickAdmissionFormPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffRelationPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffRelativeOccupationPage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffSchoolTypePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffServiceTypePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanStaffTypePage;
import com.ingenio.eprashasan.full.pages.FullEPrashasanWelcomePage;
import com.mysql.jdbc.Driver;
import com.qmetry.qaf.automation.data.MetaData;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;

public class Staff extends WebDriverTestCase{
//	Staff Setting Test Cases
	
	@Test
	@QAFDataProvider(key="StaffSetting.data")
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Save\" button functionality  without entering \"Staff School Type\"")
	public void TC_Staff_Sett_01(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();

		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		fullEPrashasanStaffSchoolTypePage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Save\" button functionality  by entering \"Staff School Type\" field data")
	public void TC_Staff_Sett_02(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys(data.get("staffschooltype"));
		fullEPrashasanStaffSchoolTypePage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		EPUtils.sleep(3000);
		Alert windowalert=getDriver().switchTo().alert();
	
		verifyTrue(windowalert.getText().contains("Data Saved Successfully"), 
				"Data Saved Successfully Alert message not displayed",
				"Data Saved Successfully Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Save\" button functionality by  entering duplicate data in the \"staff School Type\" field ")
	public void TC_Staff_Sett_03(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys(data.get("duplicate_staffschooltype"));
		fullEPrashasanStaffSchoolTypePage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
	    EPUtils.sleep(2000);
		Alert windowalert=getDriver().switchTo().alert();
	
		verifyTrue(windowalert.getText().contains("Information is already exist"), 
				"Information is already exist Alert message not displayed",
				"Information is already exist Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Update\" button functionality by without updating record")
	public void TC_Staff_Sett_04(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		
		fullEPrashasanStaffSchoolTypePage.tablerow3.click();
		fullEPrashasanStaffSchoolTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
	    EPUtils.sleep(3000);
		Alert windowalert=getDriver().switchTo().alert();
	
		verifyTrue(windowalert.getText().contains("Data is updated successfully...!"), 
				"Data is updated successfully...! Alert message not displayed",
				"Data is updated successfully...! Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Update\" button functionality by updating record with unique data")
	public void TC_Staff_Sett_05(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		
		fullEPrashasanStaffSchoolTypePage.tablerow3.click();
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys(Keys.CONTROL,"a");
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys(Keys.DELETE);
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys("High School Level");
		fullEPrashasanStaffSchoolTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
	    EPUtils.sleep(2000);
		Alert windowalert=getDriver().switchTo().alert();
	
		verifyTrue(windowalert.getText().contains("Data is updated successfully...!"), 
				"Data is updated successfully...! Alert message not displayed",
				"Data is updated successfully...! Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
	public void TC_Staff_Sett_06(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		
		fullEPrashasanStaffSchoolTypePage.tablerow3.click();
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys(Keys.CONTROL,"a");
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys(Keys.DELETE);
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys("Primary");
		fullEPrashasanStaffSchoolTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
	    EPUtils.sleep(2000);
	    
		Alert windowalert=getDriver().switchTo().alert();
	
		verifyTrue(windowalert.getText().contains("Information is already exist"), 
				"Information is already exist Alert message not displayed",
				"Information is already exist Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Reset\" button functionality")
	public void TC_Staff_Sett_07(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		
		fullEPrashasanStaffSchoolTypePage.tablerow3.click();
		fullEPrashasanStaffSchoolTypePage.textboxSchoolType.sendKeys("CBSE");
		fullEPrashasanStaffSchoolTypePage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
		public void TC_Staff_Sett_08(Map<String, String> data) {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
//		fullEPrashasanStaffBasicDetailsFormPage.waitForPageToLoad();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		fullEPrashasanStaffSchoolTypePage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
		public void TC_Staff_Sett_09(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSchoolType.click();
		
		FullEPrashasanStaffSchoolTypePage fullEPrashasanStaffSchoolTypePage = new FullEPrashasanStaffSchoolTypePage();
		fullEPrashasanStaffSchoolTypePage.tablerow3.click();
		fullEPrashasanStaffSchoolTypePage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		
	}	

	//Staff Designation Test cases
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  without entering Staff  Designation")
		public void TC_Staff_Sett_10(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
		Reporter.log("User Clicked on Staff Designation Link");
		FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
		fullEPrashasanStaffDesignationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  by entering Staff  Designation field data")
		public void TC_Staff_Sett_11(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
		Reporter.log("User Clicked on Staff Designation Link");
		FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
		fullEPrashasanStaffDesignationPage.textboxStaffDesignation.sendKeys(data.get("staffdesignation"));
		Reporter.log("User added staff designation");
		fullEPrashasanStaffDesignationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	

		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  by entering Staff  Designation field data")
		public void TC_Staff_Sett_12(Map<String, String>data){
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
		Reporter.log("User Clicked on Staff Designation Link");
		FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
		fullEPrashasanStaffDesignationPage.textboxStaffDesignation.sendKeys(data.get("duplicate_staffdesignation"));
		Reporter.log("User added duplicate staff");
		fullEPrashasanStaffDesignationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
    
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by without updating record")
		public void TC_Staff_Sett_13(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
	
		FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
		
		fullEPrashasanStaffDesignationPage.tablerow4.click();
		fullEPrashasanStaffDesignationPage.buttonUpdate.click();
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
        @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by updating record with unique data ")
		public void TC_Staff_Sett_14(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
	    FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationFormPage = new FullEPrashasanStaffDesignationPage();
		
		fullEPrashasanStaffDesignationFormPage.tablerow4.click();
		fullEPrashasanStaffDesignationFormPage.textboxStaffDesignation.clear();
		fullEPrashasanStaffDesignationFormPage.textboxStaffDesignation.sendKeys("Viceprincipal");
		fullEPrashasanStaffDesignationFormPage.buttonUpdate.click();
		EPUtils.sleep(2000);
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully "), 
						"Data Saved Successfully  Alert message not displayed",
						"Data Saved Successfully  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
        
        @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
		public void TC_Staff_Sett_15(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
		Reporter.log("User Clicked on Staff DesignationLink");
		FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
		
		fullEPrashasanStaffDesignationPage.tablerow4.click();
		fullEPrashasanStaffDesignationPage.textboxStaffDesignation.clear();
		fullEPrashasanStaffDesignationPage.textboxStaffDesignation.sendKeys("Principal");
		fullEPrashasanStaffDesignationPage.buttonUpdate.click();
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist  Alert message not displayed",
						"Information is already exist  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}

    @Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Reset\" button functionality")
	public void TC_Staff_Sett_16(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
        FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
    	Reporter.log("User Clicked on Staff DesignationLink");
		fullEPrashasanStaffDesignationPage.textboxStaffDesignation.sendKeys("Accountant");
        fullEPrashasanStaffDesignationPage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
	public void TC_Staff_Sett_17(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
        FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
        fullEPrashasanStaffDesignationPage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
    	
	@Test
	@QAFDataProvider(key="StaffSetting.data")
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
	public void TC_Staff_Sett_18(Map<String, String>data) throws InterruptedException {
    		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
//    		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDesignation.click();
		
		FullEPrashasanStaffDesignationPage fullEPrashasanStaffDesignationPage = new FullEPrashasanStaffDesignationPage();
		fullEPrashasanStaffDesignationPage.tablerow3.click();
		fullEPrashasanStaffDesignationPage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}	
// Staff Type Test cases 
    	
    	@Test
    	@QAFDataProvider(key="StaffSetting.data")
    	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
    	@QAFTestStep(description = "Validate Save button functionality  without entering Staff Type")
    	public void TC_Staff_Sett_19(Map<String, String>data) throws InterruptedException {
    		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
   		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		Reporter.log("User Clicked on Staff Type Link");
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		fullEPrashasanStaffTypePage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		}
	
    	@Test
    	@QAFDataProvider(key="StaffSetting.data")
    	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
    	@QAFTestStep(description = "Validate Save button functionality  by entering Staff Type field data")
    	public void TC_Staff_Sett_20(Map<String, String>data) throws InterruptedException {
    		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		Reporter.log("User Clicked on Staff Type Link");
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		fullEPrashasanStaffTypePage.textboxStaffType.sendKeys(data.get("stafftype"));
		Reporter.log("User added staff type");
		fullEPrashasanStaffTypePage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by  entering duplicate data in the \"Staff Type\" field ")
		public void TC_Staff_Sett_21(Map<String, String>data){
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		Reporter.log("User Clicked on Staff Type Link");
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		fullEPrashasanStaffTypePage.textboxStaffType.sendKeys(data.get("duplicate_stafftype"));
		Reporter.log("User added duplicate staff");
		fullEPrashasanStaffTypePage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by without updating record")
		public void TC_Staff_Sett_22(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		Reporter.log("User Clicked on Staff type Link");
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		
		fullEPrashasanStaffTypePage.tablerow4.click();
		fullEPrashasanStaffTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...! Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by updating record with unique data ")
		public void TC_Staff_Sett_23(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		Reporter.log("User Clicked on Staff Type Link");
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		
		fullEPrashasanStaffTypePage.tablerow4.click();
		fullEPrashasanStaffTypePage.textboxStaffType.clear();
		fullEPrashasanStaffTypePage.textboxStaffType.sendKeys("PartTime");
		fullEPrashasanStaffTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Staff update button");
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...!  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	    
    /*@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
		public void TC_Staff_Sett_24(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		Reporter.log("User Clicked on Staff Type Link");
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		
		fullEPrashasanStaffTypePage.tablerow4.click();
		fullEPrashasanStaffTypePage.textboxStaffType.clear();
		fullEPrashasanStaffTypePage.textboxStaffType.sendKeys("servent");
		fullEPrashasanStaffTypePage.buttonUpdate.click();
		
        Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist  Alert message not displayed",
						"Information is already exist  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}*/

    @Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Reset\" button functionality")
	public void TC_Staff_Sett_25(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//        		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
	    FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
    	Reporter.log("User Clicked on Staff DesignationLink");
    	fullEPrashasanStaffTypePage.textboxStaffType.sendKeys("Regular");
    	fullEPrashasanStaffTypePage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
	@Test
	@QAFDataProvider(key="StaffSetting.data" )
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
	public void TC_Staff_Sett_26(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//        		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
        fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
        FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
        fullEPrashasanStaffTypePage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
	
	@Test
	@QAFDataProvider(key="StaffSetting.data")
	@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
	public void TC_Staff_Sett_27(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));

		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffType.click();
		
		FullEPrashasanStaffTypePage fullEPrashasanStaffTypePage = new FullEPrashasanStaffTypePage();
		fullEPrashasanStaffTypePage.tablerow4.click();
		fullEPrashasanStaffTypePage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}	

		
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  without entering Staff Department")
		public void TC_Staff_Sett_28(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
			EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		Reporter.log("User Clicked on Staff Department Link");
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		fullEPrashasanStaffDepartmentPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  by entering Staff Department field data")
		public void TC_Staff_Sett_29(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		Reporter.log("User Clicked on Staff Department Link");
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.sendKeys(data.get("staffdepartment"));
		Reporter.log("User added Staff Department");
		fullEPrashasanStaffDepartmentPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by  entering duplicate data in the \"Staff Department\" field ")
		public void TC_Staff_Sett_30(Map<String, String>data){
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		Reporter.log("User Clicked on Staff Department Link");
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.sendKeys(data.get("duplicate_staffdepartment"));
		Reporter.log("User added duplicate staff");
		fullEPrashasanStaffDepartmentPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by without updating record")
		public void TC_Staff_Sett_31(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		Reporter.log("User Clicked on Staff Department Link");
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		
		fullEPrashasanStaffDepartmentPage.tablerow4.click();
		fullEPrashasanStaffDepartmentPage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by updating record with unique data ")
		public void TC_Staff_Sett_32(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		Reporter.log("User Clicked on Staff Department Link");
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		
		fullEPrashasanStaffDepartmentPage.tablerow4.click();
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.clear();
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.sendKeys("Stationary");
		fullEPrashasanStaffDepartmentPage.buttonUpdate.click();
		Reporter.log("User Clicked on Staff update button");
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	    
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
		public void TC_Staff_Sett_33(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		Reporter.log("User Clicked on Staff Department Link");
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		
		fullEPrashasanStaffDepartmentPage.tablerow4.click();
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.clear();
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.sendKeys("Teaching");
		fullEPrashasanStaffDepartmentPage.buttonUpdate.click();
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist  Alert message not displayed",
						"Information is already exist  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Reset\" button functionality")
		public void TC_Staff_Sett_34(Map<String, String> data) {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
	    FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		Reporter.log("User Clicked on Staff DesignationLink");
		fullEPrashasanStaffDepartmentPage.textboxStaffDepartment.sendKeys("Regular");
		fullEPrashasanStaffDepartmentPage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
		public void TC_Staff_Sett_35(Map<String, String> data) {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
	    FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
	    fullEPrashasanStaffDepartmentPage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
		public void TC_Staff_Sett_36(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffDepartment.click();
		
		FullEPrashasanStaffDepartmentPage fullEPrashasanStaffDepartmentPage = new FullEPrashasanStaffDepartmentPage();
		fullEPrashasanStaffDepartmentPage.tablerow4.click();
		fullEPrashasanStaffDepartmentPage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}	
	
		// Staff Relation
		
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  without entering Staff Relation")
		public void TC_Staff_Sett_37(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
			EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		Reporter.log("User Clicked on Staff Relation Link");
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		fullEPrashasanStaffRelationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  by entering Staff Relation field data")
		public void TC_Staff_Sett_38(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		Reporter.log("User Clicked on Staff Relation Link");
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		fullEPrashasanStaffRelationPage.textboxStaffRelation.sendKeys(data.get("staffrelation"));
		Reporter.log("User added Staff Relation");
		fullEPrashasanStaffRelationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by  entering duplicate data in the \"Staff Relation\" field ")
		public void TC_Staff_Sett_39(Map<String, String>data){
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		Reporter.log("User Clicked on Staff Relation Link");
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		fullEPrashasanStaffRelationPage.textboxStaffRelation.sendKeys(data.get("duplicate_staffrelation"));
		Reporter.log("User added duplicate staff relation");
		fullEPrashasanStaffRelationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist Alert message not displayed",
						"Information is already exist Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by without updating record")
		public void TC_Staff_Sett_40(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		Reporter.log("User Clicked on Staff Relation Link");
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		
		fullEPrashasanStaffRelationPage.tablerow3.click();
		fullEPrashasanStaffRelationPage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...! Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by updating record with unique data ")
		public void TC_Staff_Sett_41(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		Reporter.log("User Clicked on Staff Relation Link");
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		
		fullEPrashasanStaffRelationPage.tablerow3.click();
		fullEPrashasanStaffRelationPage.textboxStaffRelation.clear();
		fullEPrashasanStaffRelationPage.textboxStaffRelation.sendKeys("Gurdian");
		fullEPrashasanStaffRelationPage.buttonUpdate.click();
		Reporter.log("User Clicked on Staff update button");
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...!  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	    
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
		public void TC_Staff_Sett_42(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		Reporter.log("User Clicked on Staff Relation Link");
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		
		fullEPrashasanStaffRelationPage.tablerow3.click();
		fullEPrashasanStaffRelationPage.textboxStaffRelation.clear();
		fullEPrashasanStaffRelationPage.textboxStaffRelation.sendKeys("Spouse");
		fullEPrashasanStaffRelationPage.buttonUpdate.click();
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist  Alert message not displayed",
						"Information is already exist  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Reset\" button functionality")
		public void TC_Staff_Sett_43(Map<String, String> data) {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
	    FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		Reporter.log("User Clicked on Staff DesignationLink");
		fullEPrashasanStaffRelationPage.textboxStaffRelation.sendKeys("Father");
		fullEPrashasanStaffRelationPage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
		public void TC_Staff_Sett_44(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
	    FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
	    fullEPrashasanStaffRelationPage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
		public void TC_Staff_Sett_45(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelation.click();
		
		FullEPrashasanStaffRelationPage fullEPrashasanStaffRelationPage = new FullEPrashasanStaffRelationPage();
		fullEPrashasanStaffRelationPage.tablerow3.click();
		fullEPrashasanStaffRelationPage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}	
	
		// Staff relative Occupation
		
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  without entering Staff Relative Occupation")
		public void TC_Staff_Sett_46(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
			EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		fullEPrashasanStaffRelativeOccupationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  by entering Staff Relative Occupation field data")
		public void TC_Staff_Sett_47(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.sendKeys(data.get("staffrelativeoccupation"));
		Reporter.log("User added Staff Relative Occupation");
		fullEPrashasanStaffRelativeOccupationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by  entering duplicate data in the \"Staff Relative Occupation\" field ")
		public void TC_Staff_Sett_48(Map<String, String>data){
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.sendKeys(data.get("duplicate_staffrelativeoccupation"));
		Reporter.log("User added duplicate staff");
		fullEPrashasanStaffRelativeOccupationPage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by without updating record")
		public void TC_Staff_Sett_49(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		
		fullEPrashasanStaffRelativeOccupationPage.tablerow2.click();
		fullEPrashasanStaffRelativeOccupationPage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...! Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by updating record with unique data ")
		public void TC_Staff_Sett_50(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		
		fullEPrashasanStaffRelativeOccupationPage.tablerow2.click();
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.clear();
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.sendKeys("Job");
		fullEPrashasanStaffRelativeOccupationPage.buttonUpdate.click();
		Reporter.log("User Clicked on Staff update button");
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...!  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	    
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
		public void TC_Staff_Sett_51(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		
		fullEPrashasanStaffRelativeOccupationPage.tablerow2.click();
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.clear();
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.sendKeys("HouseWife");
		fullEPrashasanStaffRelativeOccupationPage.buttonUpdate.click();
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist  Alert message not displayed",
						"Information is already exist  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	    @Test
	    @QAFDataProvider(key="StaffSetting.data" )
	    @MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
	    @QAFTestStep(description = "Validate \"Reset\" button functionality")
	    public void TC_Staff_Sett_52(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
	    FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		Reporter.log("User Clicked on Staff DesignationLink");
		fullEPrashasanStaffRelativeOccupationPage.textboxStaffRelativeOccupation.sendKeys("Servent");
		fullEPrashasanStaffRelativeOccupationPage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
		public void TC_Staff_Sett_53(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//   fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
	    FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
	    fullEPrashasanStaffRelativeOccupationPage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
		public void TC_Staff_Sett_54(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffRelativeOccupation.click();
		
		FullEPrashasanStaffRelativeOccupationPage fullEPrashasanStaffRelativeOccupationPage = new FullEPrashasanStaffRelativeOccupationPage();
		fullEPrashasanStaffRelativeOccupationPage.tablerow2.click();
		fullEPrashasanStaffRelativeOccupationPage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}	
		
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  without entering Staff Service Type")
		public void TC_Staff_Sett_55(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
			EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		fullEPrashasanStaffServiceTypePage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Save button functionality  by entering Staff Relative Occupation field data")
		public void TC_Staff_Sett_56(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.sendKeys(data.get("StaffServiceType"));
		Reporter.log("User added Staff Relative Occupation");
		fullEPrashasanStaffServiceTypePage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data Saved Successfully"), 
						"Data Saved Successfully Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by  entering duplicate data in the \"Staff Relative Occupation\" field ")
		public void TC_Staff_Sett_58(Map<String, String>data){
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.sendKeys(data.get("duplicate_StaffServiceType"));
		Reporter.log("User added duplicate staff");
		fullEPrashasanStaffServiceTypePage.buttonSave.click();
		Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist Alert message not displayed",
						"Data Saved Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by without updating record")
		public void TC_Staff_Sett_59(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		
		fullEPrashasanStaffServiceTypePage.tablerow3.click();
		fullEPrashasanStaffServiceTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Update button");
		EPUtils.sleep(2000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...! Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Update button functionality by updating record with unique data ")
		public void TC_Staff_Sett_60(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff setting link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		
		fullEPrashasanStaffServiceTypePage.tablerow3.click();
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.clear();
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.sendKeys("PartTime");
		fullEPrashasanStaffServiceTypePage.buttonUpdate.click();
		Reporter.log("User Clicked on Staff update button");
		EPUtils.sleep(2000);
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Data is updated successfully...!"), 
						"Data is updated successfully...! Alert message not displayed",
						"Data is updated successfully...!  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	    
	    @Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" button functionality by  updating record with duplicate name")
		public void TC_Staff_Sett_61(Map<String, String>data) throws InterruptedException {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		Reporter.log("User Clicked on Staff Relative Occupation Link");
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		
		fullEPrashasanStaffServiceTypePage.tablerow3.click();
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.clear();
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.sendKeys("servent");
		fullEPrashasanStaffServiceTypePage.buttonUpdate.click();
		
	    Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Information is already exist"), 
						"Information is already exist  Alert message not displayed",
						"Information is already exist  Alert message displayed");
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Reset\" button functionality")
		public void TC_Staff_Sett_62(Map<String, String> data) {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
	    FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		Reporter.log("User Clicked on Staff DesignationLink");
		fullEPrashasanStaffServiceTypePage.textboxStaffServiceType.sendKeys("Regular");
		fullEPrashasanStaffServiceTypePage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
	    EPUtils.sleep(2000);
			}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionalitly by without selecting any record")
		public void TC_Staff_Sett_63(Map<String, String> data) {
			
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
	//    		fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
	    fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
	    FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
	    fullEPrashasanStaffServiceTypePage.buttonDelete.click();
	    EPUtils.sleep(2000);
	    
	    Alert windowalert=getDriver().switchTo().alert();
	    verifyTrue(windowalert.getText().contains("Please Select row to delete"),
	    "Please Select row to delete Alert message displayed",
	    "Please Select row to delete Alert message not displayed");
	    
	    getDriver().switchTo().alert().accept();
	    }
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Delete button functionalitly by deleting record which is not in use")
		public void TC_Staff_Sett_64(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffServiceType.click();
		
		FullEPrashasanStaffServiceTypePage fullEPrashasanStaffServiceTypePage = new FullEPrashasanStaffServiceTypePage();
		fullEPrashasanStaffServiceTypePage.tablerow3.click();
		fullEPrashasanStaffServiceTypePage.buttonDelete.click();
		Reporter.log("User Clicked on delete button");
		EPUtils.sleep(3000);
		
		Alert Windowalert = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert.getText().contains("Are You Sure?"), 
						"Are You Sure Alert message not displayed",
						"Are You Sure Alert message displayed");
		getDriver().switchTo().alert().accept();
		EPUtils.sleep(3000);
		Alert Windowalert1 = getDriver().switchTo().alert();
		
		verifyTrue(Windowalert1.getText().contains("Record Deleted Successfully!"), 
						"Record Deleted Successfully Alert message not displayed",
						"Record Deleted Successfully Alert message displayed");
		getDriver().switchTo().alert().accept();
		}	
	
	//	Staff Form Excel Import Export 
		
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"click here\"active link functionality")
		public void TC_Staff_Sett_65(Map<String, String>data) throws InterruptedException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffFormExcelExportImport.click();
		Reporter.log("User Clicked on StaffFormExcelExportImport Link");
		FullEPrashasanExcelImportPage fullEPrashasanExcelImportPage=new FullEPrashasanExcelImportPage();
		fullEPrashasanExcelImportPage.linkExcelImport.click();
		Reporter.log("User Clicked on click here active link to download excel");
	    	}
	      
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Choose\" button functionality by selecting .xls file")
		public void TC_Staff_Sett_66(Map<String, String>data) throws IOException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffFormExcelExportImport.click();
		Reporter.log("User Clicked on StaffFormExcelExportImport Link");
		FullEPrashasanExcelImportPage fullEPrashasanExcelImportPage=new FullEPrashasanExcelImportPage();
	
		fullEPrashasanExcelImportPage.linkExcelImport.click();
		EPUtils.sleep(3000);
		Reporter.log("User Clicked on Excel Import link");
		fullEPrashasanExcelImportPage.buttonChooseFile.sendKeys("C:\\Users\\admin\\Downloads\\Staff_Admission_Form_Excel_12_12_2019_12_38_32_281.xlsx");
		EPUtils.sleep(3000);
		Reporter.log("User choosen file");
		
	//	fullEPrashasanExcelImportPage.buttonChooseFile.click();
	//	EPUtils.sleep(2000);
	//	Reporter.log("User Clicked on Choose File");
		
	//	below line execute AutoIT script
	//	Runtime.getRuntime().exec("D:\\AutoIT-files\\StaffExcelUpload.exe");
		
	
		}
	
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Check Excel\"Button functionality if any Error")
		public void TC_Staff_Sett_67(Map<String, String>data) throws IOException {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffFormExcelExportImport.click();
		Reporter.log("User Clicked on StaffFormExcelExportImport Link");
		FullEPrashasanExcelImportPage fullEPrashasanExcelImportPage=new FullEPrashasanExcelImportPage();
	
		fullEPrashasanExcelImportPage.linkExcelImport.click();
		EPUtils.sleep(3000);
		Reporter.log("User Clicked on Excel Import link");
		fullEPrashasanExcelImportPage.buttonChooseFile.sendKeys("C:\\Users\\admin\\Downloads\\Staff_Admission_Form_Excel_12_12_2019_12_38_32_281.xlsx");
		EPUtils.sleep(3000);
		Reporter.log("User choosen file");
		fullEPrashasanExcelImportPage.buttonCheckExcel.click();
		
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Please Solve Error. Then Click On Save Button."), 
				"Please Solve Error. Then Click On Save Button. Alert message not displayed",
				"Please Solve Error. Then Click On Save Button. Alert message displayed");
	    getDriver().switchTo().alert().accept();
	
		}
	     
		@Test
		@QAFDataProvider(key="StaffSetting.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save Excel\"Button functionality if no error")
		public void TC_Staff_Sett_68(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Link");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage = new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffSetting.click();
		Reporter.log("User Clicked on Staff Setting Link");
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffFormExcelExportImport.click();
		Reporter.log("User Clicked on StaffFormExcelExportImport Link");
		FullEPrashasanExcelImportPage fullEPrashasanExcelImportPage=new FullEPrashasanExcelImportPage();
	
		fullEPrashasanExcelImportPage.linkExcelImport.click();
		EPUtils.sleep(3000);
		Reporter.log("User Clicked on Excel Import link");
		fullEPrashasanExcelImportPage.buttonChooseFile.sendKeys("C:\\Users\\admin\\Downloads\\Staff_Admission_Form_Excel_12_12_2019_12_38_32_281.xlsx");
		EPUtils.sleep(3000);
		Reporter.log("User choosen file");
		fullEPrashasanExcelImportPage.buttonCheckExcel.click();
		fullEPrashasanExcelImportPage.buttonSaveExcel.click();
		
	//	Alert windowalert=getDriver().switchTo().alert();
	//	
	//	verifyTrue(windowalert.getText().contains("Record Saved Successfully"),
	//			"Record Saved Successfully Alert message not displayed",
	//			"Record Saved Successfully Alert message displayed");
	//    getDriver().switchTo().alert().accept();
		
	}
	   

	//		Staff Basic Details
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality without entering any details")
		public void TC_Staff_StaffBasicDetails_01(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		Reporter.log("User Clicked on Staff Basic Details Page Loaded");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select Staff Role...!!"),
				"Please Select Staff Role...!!Alert message not displayed",
				"Please Select Staff Role...!!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		

		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by selecting staff role")
		public void TC_Staff_StaffBasicDetails_02(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select School Type..!"),
				"Please Select School Type..!Alert message not displayed",
				"Please Select School Type..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"save \"button functionality by selectiong staff role,staff school type ")
		public void TC_Staff_StaffBasicDetails_03(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select Joining Year..!"),
				"Please Select Joining Year..!Alert message not displayed",
				"Please Select Joining Year..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by entering staff role,school type and joing year")
		public void TC_Staff_StaffBasicDetails_04(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownJoiningYear.selectByIndex(2);
	    Reporter.log("User Selected joining year");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Enter Registration No..!"),
				"Please Enter Registration No..!Alert message not displayed",
				"Please Enter Registration No..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by entering staff role,school type,joing year,regi no.")
		public void TC_Staff_StaffBasicDetails_05(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownJoiningYear.selectByIndex(2);
	    Reporter.log("User Selected joining year");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.textboxStaffRegNo.sendKeys(data.get("staffregino"));
	    Reporter.log("User entered Staff Regi No.");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select Staff Type..!"),
				"Please Select Staff Type..!Alert message not displayed",
				"Please Select Staff Type..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by entering staff role,school type,joing year,regi no.and Staff type")
		public void TC_Staff_StaffBasicDetails_06(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownJoiningYear.selectByIndex(2);
	    Reporter.log("User Selected joining year");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.textboxStaffRegNo.sendKeys(data.get("staffregino"));
	    Reporter.log("User entered Staff Regi No.");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffType.selectByIndex(1);
	    Reporter.log("User selected staff Type");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select Staff Designation..!"),
				"Please Select Staff Designation..!Alert message not displayed",
				"Please Select Staff Designation..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by entering staff role,school type,joing year,regi no.,staff type and staff designation ")
		public void TC_Staff_StaffBasicDetails_07(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownJoiningYear.selectByIndex(2);
	    Reporter.log("User Selected joining year");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.textboxStaffRegNo.sendKeys(data.get("staffregino"));
	    Reporter.log("User entered Staff Regi No.");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffType.selectByIndex(1);
	    Reporter.log("User selected staff Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffDesignation.selectByIndex(1);
	    Reporter.log("User selected staff designation");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select Staff Department..!"),
				"Please Select Staff Department..!Alert message not displayed",
				"Please Select Staff Department..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by entering staff role,school type,joing year,regi no.,staff type, staff designation and staff department ")
		public void TC_Staff_StaffBasicDetails_08(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownJoiningYear.selectByIndex(2);
	    Reporter.log("User Selected joining year");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.textboxStaffRegNo.sendKeys(data.get("staffregino"));
	    Reporter.log("User entered Staff Regi No.");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffType.selectByIndex(1);
	    Reporter.log("User selected staff Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffDesignation.selectByIndex(2);
	    Reporter.log("User selected staff designation");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffDepartment.selectByIndex(1);
	    Reporter.log("User selected staff department");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Select Staff Service Type..!"),
				"Please Select Staff Service Type..!Alert message not displayed",
				"Please Select Staff Service Type..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffBasicDetails.data")
		@MetaData(value = "{'groups':['Staff-> Staff Basic Details Functionality']}")
		@QAFTestStep(description = "Validate \"Save button\"functionality by entering all office details")
		public void TC_Staff_StaffBasicDetails_09(Map<String, String>data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage=new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"),data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage= new FullEPrashasanWelcomePage();
	    fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();

		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
	    Reporter.log("User Clicked on Staff Basic Details Page Loaded");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffRole.selectByIndex(1);
	    Reporter.log("User Selected Staff role");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffSchoolType.selectByIndex(1);
	    Reporter.log("User Selected School Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownJoiningYear.selectByIndex(2);
	    Reporter.log("User Selected joining year");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.textboxStaffRegNo.sendKeys(data.get("staffregino"));
	    Reporter.log("User entered Staff Regi No.");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffType.selectByIndex(1);
	    Reporter.log("User selected staff Type");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffDesignation.selectByIndex(2);
	    Reporter.log("User selected staff designation");
	    fullEPrashasanStaffBasicDetailsFormPage.officeDetailsComponent.dropdownStaffServiceType.selectByIndex(1);
	    Reporter.log("User selected staff service type");
		fullEPrashasanStaffBasicDetailsFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Enter First Name..!"),
				"Please Enter First Name..!Alert message not displayed",
				"Please Enter First Name..!Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		
		//Staff Quick Admission
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality without entering  any details")
		public void Tc_Staff_StaffQuick_01(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Mandatory Fields Are Blank Or Empty"),
				"Mandatory Fields Are Blank Or Empty Alert message not displayed",
				"Mandatory Fields Are Blank Or Empty Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
	
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\"button functionality by selecting \"Staff Role\"")
		public void Tc_Staff_StaffQuick_02(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Mandatory Fields Are Blank Or Empty"),
				"Mandatory Fields Are Blank Or Empty Alert message not displayed",
				"Mandatory Fields Are Blank Or Empty Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
	    
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by entering \"First Name\"")
		public void Tc_Staff_StaffQuick_03(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Mandatory Fields Are Blank Or Empty"),
				"Mandatory Fields Are Blank Or Empty Alert message not displayed",
				"Mandatory Fields Are Blank Or Empty Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
	
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by entering \"Middle Name\"")
		public void Tc_Staff_StaffQuick_04(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Mandatory Fields Are Blank Or Empty"),
				"Mandatory Fields Are Blank Or Empty Alert message not displayed",
				"Mandatory Fields Are Blank Or Empty Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by entering \"Last Name\"")
		public void Tc_Staff_StaffQuick_05(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Mandatory Fields Are Blank Or Empty"),
				"Mandatory Fields Are Blank Or Empty Alert message not displayed",
				"Mandatory Fields Are Blank Or Empty Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Save\" button functionality by entering \"Mobile Number\"")
		public void Tc_Staff_StaffQuick_06(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownInitialName.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo.sendKeys(data.get("mobileno"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		EPUtils.sleep(2000);
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Information Saved Successfully."),
				"Information Saved Successfully. Alert message not displayed",
				"Information Saved Successfully. Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		
		GetSQLData.getSchoolMaster(data);
		GetSQLData.getstaffQuickAdmissionQuery(data);
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \" Mobile\" field validation by entering 11 digits number")
		public void Tc_Staff_StaffQuick_07(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo.sendKeys(data.get("elevendigitmobileno"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Please Enter Valid Mobile No..!"),
				"Please Enter Valid Mobile No..! Alert message not displayed",
				"Please Enter Valid Mobile No..! Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \" Mobile\" field validation by entering 12 digits number")
		public void Tc_Staff_StaffQuick_08(Map<String, String>data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		fullEPrashasanLoginPage.waitForPageToLoad();
		
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
		
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage = new FullEPrashasanWelcomePage();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
		fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
		Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(1);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo.sendKeys(data.get("twelvedigitmobileno"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
		Reporter.log("User Clicked on Save button");
		EPUtils.sleep(2000);
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Information Saved Successfully."),
				"Information Saved Successfully. Alert message not displayed",
				"Information Saved Successfully. Alert message displayed");
		
		getDriver().switchTo().alert().accept();
		}
			
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Reset\" button functionality by entering all fields ")
		public void Tc_Staff_StaffQuick_09(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//			fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		 Reporter.log("User started filling all details");
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo.sendKeys(data.get("mobileno"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonReset.click();
		Reporter.log("User Clicked on Reset button");
		EPUtils.sleep(2000);
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Verify Save button functionality by \"add more\"staff details ")
		public void Tc_Staff_StaffQuick_10(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//			fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		 Reporter.log("User started filling all details");
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(1);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo.sendKeys(data.get("mobileno"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonAddMore.click();
		Reporter.log("User Clicked on Add More button");
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole1.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName1.sendKeys(data.get("firstname_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName1.sendKeys(data.get("middlename_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName1.sendKeys(data.get("lastname_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo1.sendKeys(data.get("mobileno_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
     	Reporter.log("User Clicked on save button");
		EPUtils.sleep(3000);
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Information Saved Successfully."),
				"Information Saved Successfully. Alert message is not displayed",
				"Information Saved Successfully. Alert message is displayed");
		
		getDriver().switchTo().alert().accept();
		
		GetSQLData.getstaffQuickAdmissionQuery(data);
		
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Verify Save button functionality by \"add more\" without entering details")
		public void Tc_Staff_StaffQuick_11(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
//			fullEPrashasanLoginPage.waitForPageToLoad();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();	
		fullEPrashasanStaffQuickAdmissionFormPage.buttonAddMore.click();
		Reporter.log("User Clicked on Add More button");

		fullEPrashasanStaffQuickAdmissionFormPage.buttonSave.click();
     	Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Mandatory Fields Are Blank Or Empty"),
				"Mandatory Fields Are Blank Or Empty Alert message is not displayed",
				"Mandatory Fields Are Blank Or Empty Alert message is displayed");
		
		getDriver().switchTo().alert().accept();
		
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Verify \"Reset\" button functionality by \"add more\"staff details ")
		public void Tc_Staff_StaffQuick_12(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		 Reporter.log("User started filling all details");
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(1);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName.sendKeys(data.get("firstname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys(data.get("middlename"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName.sendKeys(data.get("lastname"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo.sendKeys(data.get("mobileno"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonAddMore.click();
		Reporter.log("User Clicked on Add More button");
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole1.selectByIndex(2);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxFirstName1.sendKeys(data.get("firstname_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName1.sendKeys(data.get("middlename_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxLastName1.sendKeys(data.get("lastname_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMobileNo1.sendKeys(data.get("mobileno_1"));
		fullEPrashasanStaffQuickAdmissionFormPage.buttonReset.click();
     	Reporter.log("User Clicked on save button");
		EPUtils.sleep(2000);
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Verify \"Reset\" button functionality by \"add more\"without entering staff details")
		public void Tc_Staff_StaffQuick_13(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonAddMore.click();
		Reporter.log("User Clicked on Add More button");
	    fullEPrashasanStaffQuickAdmissionFormPage.buttonReset.click();
     	Reporter.log("User Clicked on Reset button");
		EPUtils.sleep(2000);
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(1);
		}
			
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff Quick Admission Functionaltiy']}")
		@QAFTestStep(description = "Verify \"Remove\"link by adding staff admission row using 'Add More'button")
		public void Tc_Staff_StaffQuick_14(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonAddMore.click();
		Reporter.log("User Clicked on Add More button");
	    fullEPrashasanStaffQuickAdmissionFormPage.linkRemove.click();
     	Reporter.log("User Clicked on Remove link");
		EPUtils.sleep(2000);
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.selectByIndex(1);
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"For Edit Click Here\" link functionality link functionality")
		public void Tc_Staff_StaffQuick_15(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		EPUtils.sleep(2000);
	    fullEPrashasanStaffQuickAdmissionFormPage.textfieldRow.click();
     	Reporter.log("User Clicked on first row of staff information grid ");
		EPUtils.sleep(2000);
			}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Edit\" functionality of Staff Quick Admission form by without selecting record")
		public void Tc_Staff_StaffQuick_16(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		fullEPrashasanStaffQuickAdmissionFormPage.buttonEdit.click();
		Reporter.log("User Clicked on Edit button");
		EPUtils.sleep(2000);
	    }
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" functionality of Staff Quick Admission form by without editing selected record")
		public void Tc_Staff_StaffQuick_17(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		fullEPrashasanStaffQuickAdmissionFormPage.textfieldRow.click();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonEdit.click();
		Reporter.log("User Clicked on Edit button");
		EPUtils.sleep(2000);
	    fullEPrashasanStaffQuickAdmissionFormPage.buttonUpdate.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on Update button");
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Kindly edit the details"),
		    "Kindly edit the details Alert message is not displayed",
            "Kindly edit the details Alert message is displayed");
		
		getDriver().switchTo().alert().accept();
		}

		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Update\" functionality of Staff Quick Admission form by editing selected record")
		public void Tc_Staff_StaffQuick_18(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		fullEPrashasanStaffQuickAdmissionFormPage.textfieldRow.click();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonEdit.click();
		Reporter.log("User Clicked on Edit button");
		EPUtils.sleep(2000);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.clear();
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys("Jignesh");
	    fullEPrashasanStaffQuickAdmissionFormPage.buttonUpdate.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on Update button");
		Alert windowalert=getDriver().switchTo().alert();
		verifyTrue(windowalert.getText().contains("Information Updated Successfully"),
		    "Information Updated Successfully Alert message is not displayed",
            "Information Updated Successfully Alert message is displayed");
		
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"reset\" functionality of Staff Quick Admission form by editing selected record")
		public void Tc_Staff_StaffQuick_19(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new 
        FullEPrashasanStaffBasicDetailsFormPage();
		
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		fullEPrashasanStaffQuickAdmissionFormPage.textfieldRow.click();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonEdit.click();
		Reporter.log("User Clicked on Edit button");
		EPUtils.sleep(2000);
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.clear();
		fullEPrashasanStaffQuickAdmissionFormPage.textboxMiddleName.sendKeys("Jignesh");
	    fullEPrashasanStaffQuickAdmissionFormPage.buttonReset.click();
		EPUtils.sleep(2000);
		Reporter.log("User Clicked on Reset button");
		fullEPrashasanStaffQuickAdmissionFormPage.dropdownStaffRole.click();
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionality by without selecting any record")
		public void Tc_Staff_StaffQuick_20(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		EPUtils.sleep(2000);
		fullEPrashasanStaffQuickAdmissionFormPage.buttonDelete.click();
		Reporter.log("User Clicked on Delete button");
		EPUtils.sleep(2000);

//		 In ERP, there is no such alert
 
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Please Select Record For Delete"),
				"Please Select Record For Delete Alert message not displayed",
				"Please Select Record For Delete Alert message not displayed");
		}
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data" )
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionality by selecting record which is in use")
		public void Tc_Staff_StaffQuick_21(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		fullEPrashasanStaffQuickAdmissionFormPage.textfieldRow.click();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonDelete.click();
		Reporter.log("User Clicked on Delete button");
		EPUtils.sleep(2000);

//		 In ERP,"Are You Sure? You Want To Delete This?" alert is displayed,record should not get deleted 
 
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Record is in use,cannot be deleted"),
				"Record is in use,cannot be deleted Alert message not displayed",
				"Record is in use,cannot be deleted Alert message not displayed");
         }
		
		@Test
		@QAFDataProvider(key="StaffQuickAdmission.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate \"Delete\" button functionality by selecting record which is not in use")
		public void Tc_Staff_StaffQuick_22(Map<String, String> data) {
		
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkStaffQuickAdmission.click();
		Reporter.log("User Clicked on Staff Quick Admission Link");
		FullEPrashasanStaffQuickAdmissionFormPage fullEPrashasanStaffQuickAdmissionFormPage = new 
        FullEPrashasanStaffQuickAdmissionFormPage();
		fullEPrashasanStaffQuickAdmissionFormPage.linkEditHere.click();
		Reporter.log("User Clicked on Edit Here link");
		fullEPrashasanStaffQuickAdmissionFormPage.textfieldRow.click();
		fullEPrashasanStaffQuickAdmissionFormPage.buttonDelete.click();
		Reporter.log("User Clicked on Delete button");
		EPUtils.sleep(2000);
 
		Alert windowalert=getDriver().switchTo().alert();
		
		verifyTrue(windowalert.getText().contains("Are You Sure? You Want To Delete This?"),
				"Are You Sure? You Want To Delete This? Alert message not displayed",
				"Are You Sure? You Want To Delete This? Alert message displayed");
		getDriver().switchTo().alert().accept();
		
       Alert windowalert1=getDriver().switchTo().alert();
		
		verifyTrue(windowalert1.getText().contains("Information Deleted Successfully..!!"),
				"Information Deleted Successfully..!! Alert message not displayed",
				"Information Deleted Successfully..!! Alert message not displayed");
		getDriver().switchTo().alert().accept();
		}
		
		@Test
		@QAFDataProvider(key="AssignSubjectToStaff.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Academic year dropdown list by selecting year for which record is exist ")
		public void Tc_Staff_AssignSubToStaff_01(Map<String, String> data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkAssignSubjectToStaff.click();
		Reporter.log("User Clicked on Assign Subject to Staff link");
		FullEPrashasanAssignSubjectToStaff fullEPrashasanAssignSubjectToStaff=new FullEPrashasanAssignSubjectToStaff();
		fullEPrashasanAssignSubjectToStaff.dropdownYear.selectByIndex(2);
		Reporter.log("User Clicked on Academic Year");
		fullEPrashasanAssignSubjectToStaff.tableStaffSubjectDetails.isDisplayed();
	    Reporter.log("Verified Staff subject details table is visible");
		}
		
		@Test
		@QAFDataProvider(key="AssignSubjectToStaff.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Academic year dropdown list by selecting year for which record is not exist ")
		public void Tc_Staff_AssignSubToStaff_02(Map<String, String> data) {
	    FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkAssignSubjectToStaff.click();
		Reporter.log("User Clicked on Assign Subject to Staff link");
		FullEPrashasanAssignSubjectToStaff fullEPrashasanAssignSubjectToStaff=new FullEPrashasanAssignSubjectToStaff();
		EPUtils.sleep(3000);
		fullEPrashasanAssignSubjectToStaff.dropdownYear.selectByIndex(2);
		Reporter.log("User Clicked on Academic Year");
		EPUtils.sleep(2000);
	
			if(fullEPrashasanAssignSubjectToStaff.tableStaffSubjectDetails.getText().isEmpty())
			{
				Reporter.log("verify blank record");
			}else 
			{
				Reporter.log("Verify No blank record");
				System.err.println("Failed To Execute");
			}
		}
		
		@Test
		@QAFDataProvider(key="AssignSubjectToStaff.data")
		@MetaData(value = "{'groups':['Staff-> Setting Functionaltiy']}")
		@QAFTestStep(description = "Validate Academic year dropdown list by selecting year for which record is not exist ")
		public void Tc_Staff_AssignSubToStaff_03(Map<String, String> data) {
		FullEPrashasanLoginPage fullEPrashasanLoginPage = new FullEPrashasanLoginPage();
		fullEPrashasanLoginPage.invoke();
		EPUtils.loginToEPrashasan(data.get("username"), data.get("password"));
	    EPUtils.sleep(2000);
		FullEPrashasanWelcomePage fullEPrashasanWelcomePage =new FullEPrashasanWelcomePage();	
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.waitForVisible();
        fullEPrashasanWelcomePage.leftNavigationComponent.linkStaff.click();
        Reporter.log("User Clicked on Staff Module");
		FullEPrashasanStaffBasicDetailsFormPage fullEPrashasanStaffBasicDetailsFormPage=new FullEPrashasanStaffBasicDetailsFormPage();
		fullEPrashasanStaffBasicDetailsFormPage.topNavigationComponent.linkAssignSubjectToStaff.click();
		Reporter.log("User Clicked on Assign Subject to Staff link");
		FullEPrashasanAssignSubjectToStaff fullEPrashasanAssignSubjectToStaff=new FullEPrashasanAssignSubjectToStaff();
		EPUtils.sleep(3000);
		
		fullEPrashasanAssignSubjectToStaff.dropdownYear.selectByIndex(2);
		EPUtils.sleep(3000);
		
        /*fullEPrashasanAssignSubjectToStaff.textboxStaffName.sendKeys(data.get("staffname"));
		Reporter.log("User Entered staff name");
		EPUtils.sleep(2000);*/
//		fullEPrashasanAssignSubjectToStaff.textboxStaffName.clear();
//     	Reporter.log("User Clicked to clear staff name textbox");
	
		/*fullEPrashasanAssignSubjectToStaff.selectOptionWithText(); 
		Reporter.log("User Clicked on autoselect functionality");
		EPUtils.sleep(2000);*/
		
		fullEPrashasanAssignSubjectToStaff.textboxStaffName.click();
		EPUtils.sleep(2000);
		fullEPrashasanAssignSubjectToStaff.verifytextboxcolor();
	
		

		
		}
		}
		
		

